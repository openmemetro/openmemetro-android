# Memetro app

Fork from https://github.com/Memetro/memetro-android-app

See https://memetro.net/

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/com.memetro.android/)

## ¿Qué es el Trastorno Memetro?

Memetro es un tipo trastorno del funcionamiento de la memoria, durante el cual la persona afectada es incapaz de recordar que según la normativa vigente de los transportes metropolitanos, ha de validar el título de transporte en la entrada de las instalaciones.
El Memetro puede ser espontáneo, en el caso del Memetro transitorio global (TGM por sus siglas en inglés) suele durar un máximo de 75 minutos y a la larga suele derivar en congénito.
