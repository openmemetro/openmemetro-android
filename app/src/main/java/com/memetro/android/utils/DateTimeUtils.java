package com.memetro.android.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateTimeUtils {

    public static String toLocalDateFormatted(Long input, SimpleDateFormat outputFormat) throws ParseException {
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'", Locale.US);
        inputFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        Date date = new Date(input);
        outputFormat.setTimeZone(TimeZone.getDefault());
        return outputFormat.format(date);
    }

    public static String toLocalDateFormatted(Date input, SimpleDateFormat outputFormat) throws ParseException {
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'", Locale.US);
        inputFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        outputFormat.setTimeZone(TimeZone.getDefault());
        return outputFormat.format(input);
    }

    public static Date toDate(String input) throws ParseException {
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'", Locale.US);
        inputFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        return inputFormat.parse(input);
    }

    public static String toLocalDateFormatted(Long input) throws ParseException {
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'", Locale.US);
        inputFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        return inputFormat.format(new Date(input));
    }
}
