package com.memetro.android.local.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.memetro.android.api.sync.models.Transport;

import java.util.List;

@Dao
public interface TransportDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insert(Transport transport);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertAll(List<Transport> transports);

    @Query("SELECT * FROM transport WHERE transport.id = :transportId")
    public Transport getTransportById(Integer transportId);

    @Query("SELECT transport.* FROM transport INNER JOIN citiestransport ON transport.id = citiestransport.transport_id WHERE citiestransport.city_id = :cityId")
    public List<Transport> getAllTransport(int cityId);

    @Query("SELECT * FROM transport")
    public LiveData<List<Transport>> getAllTransport();
}
