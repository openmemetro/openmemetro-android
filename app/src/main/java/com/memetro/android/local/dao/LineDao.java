package com.memetro.android.local.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.memetro.android.api.sync.models.Line;

import java.util.List;

@Dao
public interface LineDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insert(Line country);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertAll(List<Line> lines);

    @Query("SELECT * FROM line WHERE line.id = :lineId")
    public Line getLineById(Integer lineId);

    @Query("SELECT * FROM line WHERE line.transport_id = :transportId")
    public List<Line> getLineByTransportId(int transportId);

    @Query("SELECT * FROM line")
    public LiveData<List<Line>> getAllLines();
}
