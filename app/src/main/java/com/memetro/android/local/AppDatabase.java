package com.memetro.android.local;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.memetro.android.api.alerts.models.Alert;
import com.memetro.android.api.alerts.models.converters.DateToTimeStampConverter;
import com.memetro.android.api.banner.models.BannerDataModel;
import com.memetro.android.api.sync.models.AlertType;
import com.memetro.android.api.sync.models.CitiesTransport;
import com.memetro.android.api.sync.models.City;
import com.memetro.android.api.sync.models.Country;
import com.memetro.android.api.sync.models.Line;
import com.memetro.android.api.sync.models.LineStation;
import com.memetro.android.api.sync.models.Station;
import com.memetro.android.api.sync.models.Transport;
import com.memetro.android.local.dao.*;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {
        City.class,
        CitiesTransport.class,
        Country.class,
        Line.class,
        LineStation.class,
        Station.class,
        Transport.class,
        Alert.class,
        AlertType.class
}, version = 7, exportSchema = false)
@TypeConverters(
        DateToTimeStampConverter.class
)
public abstract class AppDatabase extends RoomDatabase {

    public abstract CityDao cityDao();
    public abstract CitiesTransportDao citiesTransportDao();
    public abstract CountryDao countryDao();
    public abstract LineDao lineDao();
    public abstract LineStationDao lineStationDao();
    public abstract StationDao stationDao();
    public abstract TransportDao transportDao();
    public abstract AlertsDao alertsDao();
    public abstract AlertTypeDao alertTypeDao();

    private static volatile AppDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 4;
    static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    public static AppDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (AppDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase.class, "memetro_database")
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}
