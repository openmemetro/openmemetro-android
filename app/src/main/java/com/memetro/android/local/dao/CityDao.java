package com.memetro.android.local.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.memetro.android.api.sync.models.City;

import java.util.List;

@Dao
public interface CityDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insert(City city);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertAll(List<City> cities);

    @Query("SELECT * FROM city WHERE city.id = :cityId")
    public City getCityById(Integer cityId);

    @Query("SELECT * FROM city")
    public LiveData<List<City>> getAllCities();

    @Query("SELECT * FROM city WHERE country_id = :countryId")
    public List<City> getCitiesByCountryId(Integer countryId);
}
