package com.memetro.android.local.datasource.alert;

import androidx.lifecycle.LiveData;

import com.memetro.android.api.alerts.models.Alert;
import com.memetro.android.local.dao.AlertsDao;
import com.memetro.android.local.dao.models.UIAlert;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class AlertsLocalDatasource {

    private final AlertsDao alertsDao;

    public AlertsLocalDatasource(AlertsDao alertsDao) {
        this.alertsDao = alertsDao;
    }

    public List<UIAlert> getAllAlerts() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
//        cal.add(Calendar.HOUR, -24);
        // Show alerts from 3:00 a.m.
        cal.set(Calendar.HOUR_OF_DAY, 3);
        cal.set(Calendar.MINUTE, 0);
        return alertsDao.getAllAlerts(
                new Date().getTime(),
                cal.getTime().getTime()
        );
    }

    public void insertAllAlerts(List<Alert> alerts) {
        alertsDao.insertAll(alerts);
    }

    public Alert getLastAlert() {
        return alertsDao.getLastAlert();
    }
}
