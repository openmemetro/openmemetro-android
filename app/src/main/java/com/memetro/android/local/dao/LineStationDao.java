package com.memetro.android.local.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.memetro.android.api.sync.models.LineStation;

import java.util.List;

@Dao
public interface LineStationDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insert(LineStation lineStation);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertAll(List<LineStation> lineStations);

    @Query("SELECT * FROM LineStation WHERE linestation.line_id = :lineStationId")
    public LineStation getLineStationById(Integer lineStationId);

    @Query("SELECT * FROM LineStation")
    public LiveData<List<LineStation>> getAllLineStations();
}
