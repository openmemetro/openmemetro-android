package com.memetro.android.local.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.memetro.android.api.sync.models.CitiesTransport;

import java.util.List;

@Dao
public interface CitiesTransportDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insert(CitiesTransport cityTransport);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertAll(List<CitiesTransport> citiesTransports);

    @Query("SELECT * FROM citiestransport WHERE citiestransport.city_id = :cityTransportId")
    public CitiesTransport getCityTransportById(Integer cityTransportId);

    @Query("SELECT * FROM citiestransport")
    public LiveData<List<CitiesTransport>> getAllCitiesTransports();

}
