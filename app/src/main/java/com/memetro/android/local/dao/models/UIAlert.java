package com.memetro.android.local.dao.models;

import androidx.room.Embedded;
import androidx.room.Relation;

import com.memetro.android.api.alerts.models.Alert;
import com.memetro.android.api.sync.models.AlertType;
import com.memetro.android.api.sync.models.City;
import com.memetro.android.api.sync.models.Line;
import com.memetro.android.api.sync.models.Station;
import com.memetro.android.api.sync.models.Transport;

public class UIAlert {
    
    @Embedded
    Alert alert;

    @Relation(parentColumn = "alert_type_id", entityColumn = "id")
    AlertType alertType;

    @Relation(parentColumn = "line_id", entityColumn = "id")
    Line line;

    @Relation(parentColumn = "city_id", entityColumn = "id")
    City city;

    @Relation(parentColumn = "transport_id", entityColumn = "id")
    Transport transport;

    @Relation(parentColumn = "station_id", entityColumn = "id")
    Station station;

    public Alert getAlert() {
        return alert;
    }

    public void setAlert(Alert alert) {
        this.alert = alert;
    }

    public Line getLine() {
        return line;
    }

    public void setLine(Line line) {
        this.line = line;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Transport getTransport() {
        return transport;
    }

    public void setTransport(Transport transport) {
        this.transport = transport;
    }

    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    public AlertType getAlertType() {
        return alertType;
    }

    public void setAlertType(AlertType alertType) {
        this.alertType = alertType;
    }
}
