package com.memetro.android.local.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.memetro.android.api.sync.models.Station;

import java.util.List;

@Dao
public interface StationDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insert(Station station);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertAll(List<Station> stations);

    @Query("SELECT * FROM station WHERE station.id = :stationId")
    public Station getStationById(Integer stationId);

    @Query("SELECT station.* FROM station INNER JOIN linestation ON station.id = linestation.station_id WHERE linestation.line_id = :lineId"  )
    public List<Station> getAllStationsByLineId(int lineId);

    @Query("SELECT * FROM station")
    public LiveData<List<Station>> getAllStations();
}
