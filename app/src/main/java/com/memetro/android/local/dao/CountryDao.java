package com.memetro.android.local.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.memetro.android.api.sync.models.Country;

import java.util.List;

@Dao
public interface CountryDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insert(Country country);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertAll(List<Country> countries);

    @Query("SELECT * FROM country WHERE country.id = :countryId")
    public Country getCountryById(Integer countryId);

    @Query("SELECT * FROM country")
    public LiveData<List<Country>> getAllCountries();
}
