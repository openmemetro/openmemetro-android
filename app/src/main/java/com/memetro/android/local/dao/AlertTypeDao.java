package com.memetro.android.local.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.memetro.android.api.sync.models.AlertType;
import com.memetro.android.api.sync.models.AlertTypes;
import com.memetro.android.api.sync.models.Line;

import java.util.List;

@Dao
public interface AlertTypeDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insert(AlertType country);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertAll(List<AlertType> lines);

    @Query("SELECT * FROM alerttype WHERE alerttype.id = :alertTypeId")
    public AlertType getAlertTypeById(Integer alertTypeId);

    @Query("SELECT * FROM alerttype")
    public LiveData<List<AlertType>> getAllAlertTypes();
}
