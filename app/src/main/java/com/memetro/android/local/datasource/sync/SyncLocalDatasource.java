package com.memetro.android.local.datasource.sync;

import androidx.lifecycle.LiveData;

import com.memetro.android.api.sync.models.AlertType;
import com.memetro.android.api.sync.models.Data;
import com.memetro.android.api.sync.models.StaticDataResponseModel;
import com.memetro.android.api.sync.models.SyncResponseModel;
import com.memetro.android.api.sync.models.Transport;
import com.memetro.android.local.dao.AlertTypeDao;
import com.memetro.android.local.dao.CitiesTransportDao;
import com.memetro.android.local.dao.CityDao;
import com.memetro.android.local.dao.CountryDao;
import com.memetro.android.local.dao.LineDao;
import com.memetro.android.local.dao.LineStationDao;
import com.memetro.android.local.dao.StationDao;
import com.memetro.android.local.dao.TransportDao;
import com.memetro.android.sharedprefs.SharedPrefs;

import java.util.List;

public class SyncLocalDatasource {

    private final CitiesTransportDao citiesTransportDao;
    private final CityDao cityDao;
    private final CountryDao countryDao;
    private final LineDao lineDao;
    private final LineStationDao lineStationDao;
    private final StationDao stationDao;
    private final TransportDao transportDao;
    private final SharedPrefs sharedPrefs;
    private final AlertTypeDao alertTypeDao;

    public SyncLocalDatasource(CitiesTransportDao citiesTransportDao, CityDao cityDao, CountryDao countryDao, LineDao lineDao, LineStationDao lineStationDao, StationDao stationDao, TransportDao transportDao, AlertTypeDao alertTypeDao, SharedPrefs sharedPrefs) {
        this.citiesTransportDao = citiesTransportDao;
        this.cityDao = cityDao;
        this.countryDao = countryDao;
        this.lineDao = lineDao;
        this.lineStationDao = lineStationDao;
        this.stationDao = stationDao;
        this.transportDao = transportDao;
        this.sharedPrefs = sharedPrefs;
        this.alertTypeDao = alertTypeDao;
    }

    public void sync(SyncResponseModel syncResponseModel) {
        Data syncData = syncResponseModel.getData();
        //Save user
        sharedPrefs.saveUser(syncData.getUser());
        //Save countries
        countryDao.insertAll(syncData.getCountry().getData());
        //Save cities
        cityDao.insertAll(syncData.getCity().getData());
        //Save Transports
        transportDao.insertAll(syncData.getTransport().getData());
            //Save CitiesTransports FK
            citiesTransportDao.insertAll(syncData.getCitiestransport().getData());
        //Save station
        stationDao.insertAll(syncData.getStation().getData());
        //Save Lines
        lineDao.insertAll(syncData.getLine().getData());
            //Save LineStations FK
            lineStationDao.insertAll(syncData.getLinesstations().getData());
        alertTypeDao.insertAll(syncData.getAlertTypes().getData());
    }

    public LiveData<List<Transport>> getTransports() {
        return transportDao.getAllTransport();
    }

    public LiveData<List<AlertType>> getAlertTypes() {
        return alertTypeDao.getAllAlertTypes();
    }

    public void sync(StaticDataResponseModel staticDataResponseModel) {
        countryDao.insertAll(staticDataResponseModel.getCountries().getData());
        //Save cities
        cityDao.insertAll(staticDataResponseModel.getCities().getData());
    }
}
