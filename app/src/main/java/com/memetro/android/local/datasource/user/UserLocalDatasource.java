package com.memetro.android.local.datasource.user;

import com.memetro.android.api.sync.models.User;
import com.memetro.android.sharedprefs.SharedPrefs;

public class UserLocalDatasource {

    private final SharedPrefs sharedPrefs;

    public UserLocalDatasource(SharedPrefs sharedPrefs) {
        this.sharedPrefs = sharedPrefs;
    }

    public User getUser() {
        return sharedPrefs.getUser();
    }

    public void logout() {
        sharedPrefs.clearUserData();
    }

}
