package com.memetro.android.local.dao;

import static com.memetro.android.ui.dashboard.DashboardFilterViewModel.*;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.memetro.android.api.alerts.models.Alert;
import com.memetro.android.local.dao.models.UIAlert;
import com.memetro.android.ui.dashboard.DashboardFilterViewModel;

import java.util.List;

@Dao
public interface AlertsDao {

    /** TEST LIVEDATA
     insert into alert
     values (10,'Alert','2021-01-10 05:27:51','usern','41.2123123','-32.1123123',3333,2222,'Test livedata',5555,'IDK')
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insert(Alert alert);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertAll(List<Alert> alerts);

    @Query("SELECT * FROM alert WHERE alert.id = :alertId AND alert.active = 1 ")
    public Alert getAlertById(Integer alertId);

    @Transaction
    @Query("SELECT * FROM alert WHERE alert.active = 1 AND date BETWEEN :endDate AND :startDate  ORDER BY alert.id DESC")
    public List<UIAlert> getAllAlerts(Long startDate, Long endDate);

    @Transaction
    @Query("SELECT * FROM alert WHERE alert.active = 1 AND alert.transport_id IN (:transportIds) AND date BETWEEN :endDate AND :startDate ORDER BY alert.id DESC")
    public List<UIAlert> getFiltredAlerts(List<Integer> transportIds, Long startDate, Long endDate);

    @Query("SELECT * from alert WHERE alert.active = 1 ORDER BY id DESC LIMIT 1")
    public Alert getLastAlert();
}
