package com.memetro.android.ui.alerts.addalert.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.memetro.android.api.sync.models.Station;

import java.util.ArrayList;
import java.util.List;

public class StationSearchAdapter extends ArrayAdapter<Station> {

    Context context;
    int textViewResourceId;
    List<Station> items, tempItems, suggestions;

    public StationSearchAdapter(Context context, int textViewResourceId, List<Station> items) {
        super(context, textViewResourceId, items);
        this.context = context;
        this.textViewResourceId = textViewResourceId;
        this.items = items;
        tempItems = new ArrayList<Station>(items); // this makes the difference.
        suggestions = new ArrayList<Station>();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        }
        Station station = items.get(position);
        if (station != null) {
            TextView lblName = (TextView) view.findViewById(android.R.id.text1);
            if (lblName != null)
                lblName.setText(station.getName());
        }
        return view;
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    /**
     * Custom Filter implementation for custom suggestions we provide.
     */
    Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return ((Station) resultValue).getName();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                for (Station station : tempItems) {
                    if (station.getName().toLowerCase().matches("^.*"+constraint+".*$")) {
                        suggestions.add(station);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            List<Station> filterList = (ArrayList<Station>) results.values;
            if (results != null && results.count > 0) {
                clear();
                for (Station station : filterList) {
                    add(station);
                    notifyDataSetChanged();
                }
            }
        }
    };
}
