/*
 * Copyright 2013 Nytyr [me at nytyr dot me]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.memetro.android.ui.alerts.thermometer.adapter;

import android.view.View;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.memetro.android.R;
import com.memetro.android.databinding.ItemAlertThermometerBinding;
import com.memetro.android.api.alerts.models.Alert;
import com.memetro.android.local.dao.models.UIAlert;
import com.memetro.android.utils.DateTimeUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class AlertsViewHolder extends RecyclerView.ViewHolder {

    private ItemAlertThermometerBinding itemBinding;

    public AlertsViewHolder(ItemAlertThermometerBinding holderView) {
        super(holderView.getRoot());
        this.itemBinding = holderView;
    }

    public void bind(UIAlert item) {
        if(!item.getAlertType().getColor().isEmpty()) {
            itemBinding.alertTypeIcon.setText(item.getAlertType().getColor());
        }

        if(item.getStation().getName().equals("no_station")) {
            itemBinding.alertText.setText(item.getTransport().getName() + " - " + item.getLine().getName());
        }
        else if(item.getStation().getName().equals("Totes")) {
            itemBinding.alertText.setText( item.getTransport().getName() + " " + item.getLine().getName() + " - Totes");
        }
        else {
            itemBinding.alertText.setText(item.getLine().getName() + " - " + item.getStation().getName());
        }

        itemBinding.alertIcon.setImageDrawable(ContextCompat.getDrawable(
                itemBinding.getRoot().getContext(),
                item.getTransport().getIcon().iconRef
        ));

        try {
            itemBinding.hourText.setText(DateTimeUtils.toLocalDateFormatted(item.getAlert().getDate(), new SimpleDateFormat("HH:mm", Locale.getDefault())));
            itemBinding.hourText.setBackgroundResource(getBgFromHour(item.getAlert().getDate()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        try {
            itemBinding.dateText.setText(DateTimeUtils.toLocalDateFormatted(item.getAlert().getDate(), new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        itemBinding.descriptionLayout.setVisibility(item.getAlert().isExpanded() ? View.VISIBLE : View.GONE);
        if(item.getAlert().isExpanded()) {
            itemBinding.textTweet.setText(item.getAlert().getDescription());
            itemBinding.usernameTweet.setText("Anónim@");
            if(item.getAlert().getImgUrl() != null) {
                Glide.with(itemBinding.getRoot().getContext())
                        .load(item.getAlert().getImgUrl())
                        .circleCrop()
                        .into(itemBinding.avatarTweet);
            }
        }
    }

    private int getBgFromHour(Date date) {
        long diffInMills = Math.abs(new Date().getTime() - date.getTime());
        long diffHours = TimeUnit.HOURS.convert(diffInMills, TimeUnit.MILLISECONDS);

        if(diffHours <= 1.5) { return R.drawable.thermometer_red; }

        else if(diffHours <= 3.5){ return R.drawable.thermometer_orange; }

        return R.drawable.thermometer_yellow;
    }
}
