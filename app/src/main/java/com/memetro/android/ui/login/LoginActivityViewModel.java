package com.memetro.android.ui.login;

import android.os.AsyncTask;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.memetro.android.api.ResultState;
import com.memetro.android.api.login.LoginRepository;
import com.memetro.android.api.login.models.LoginRequestModel;
import com.memetro.android.api.login.models.LoginResponseModel;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;

@HiltViewModel
public class LoginActivityViewModel extends ViewModel {
    private final LoginRepository loginRepository;

    @Inject
    public LoginActivityViewModel(LoginRepository loginDatasource) {
        this.loginRepository = loginDatasource;
    }

    private final MutableLiveData<ResultState<LoginResponseModel>> _loginState = new MutableLiveData<>();
    public LiveData<ResultState<LoginResponseModel>> loginSate = _loginState;

    public void login(String username, String password, String deviceId) {
        LoginRequestModel request = new LoginRequestModel();
        request.setUsername(username);
        request.setPassword(password);
        request.setDeviceId(deviceId);
        AsyncTask.execute(() -> {
            _loginState.postValue(ResultState.loading());
            ResultState<LoginResponseModel> result =
                    loginRepository.login(
                            request
                    );
            _loginState.postValue(result);
        });
    }

    public void setAutologin(boolean autologin) {
        loginRepository.setAutologin(autologin);
    }

}
