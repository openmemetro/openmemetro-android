package com.memetro.android.ui;


import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.viewbinding.ViewBinding;

public abstract class BaseActivity<VBinding> extends FragmentActivity {

    protected Context context;
    protected VBinding binding;
    protected abstract VBinding getBinding();

    protected abstract void observeViewModel();
    protected abstract void setUpViews(@Nullable Bundle savedInstanceState);
    protected abstract void initViewModels();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getApplicationContext();
        binding = getBinding();
        setContentView(((ViewBinding)binding).getRoot());
        initViewModels();
        observeViewModel();
        setUpViews(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        observeViewModel();
    }
}

