package com.memetro.android.ui.alerts.addalert;

import android.os.AsyncTask;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.memetro.android.api.ResultState;
import com.memetro.android.api.alerts.AlertsRepository;
import com.memetro.android.api.alerts.models.AlertCreationRequest;
import com.memetro.android.api.alerts.models.AlertCreationResponse;
import com.memetro.android.api.sync.models.*;
import com.memetro.android.local.dao.*;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;
import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;

@HiltViewModel
public class AddAlertViewModel extends ViewModel {

    private final CityDao cityDao;
    private final LineDao lineDao;
    private final StationDao stationDao;
    private final TransportDao transportDao;
    private final AlertTypeDao alertTypeDao;
    private final AlertsRepository alertsRepository;

    @Inject
    public AddAlertViewModel(CityDao cityDao, LineDao lineDao, StationDao stationDao, TransportDao transportDao, AlertTypeDao alertTypeDao, AlertsRepository alertsRepository) {
        this.cityDao = cityDao;
        this.lineDao = lineDao;
        this.stationDao = stationDao;
        this.transportDao = transportDao;
        this.alertTypeDao = alertTypeDao;
        this.alertsRepository = alertsRepository;
        getCities();
        getAlertTypes();
    }

    public LiveData<List<City>> cities = new MutableLiveData<>();

    public void getCities() {
        cities = cityDao.getAllCities();
    }

    private final MutableLiveData<List<Transport>> _transport = new MutableLiveData<>();
    public LiveData<List<Transport>> transport = _transport;

    public void getTransports(@Nullable Integer cityId) {
        AsyncTask.execute(() ->  {
            if(cityId == null) {
                _transport.postValue(new ArrayList<>());
            } else {
                _transport.postValue(transportDao.getAllTransport(cityId));
            }
        });
    }

    private final MutableLiveData<List<Line>> _lines = new MutableLiveData<>();
    public LiveData<List<Line>> lines = _lines;

    public void getLines(@Nullable Integer transportId) {
        AsyncTask.execute(() -> {
            if (transportId == null) {
                _lines.postValue(new ArrayList<>());
            } else {
                _lines.postValue(lineDao.getLineByTransportId(transportId));
            }
        });
    }

    private final MutableLiveData<List<Station>> _stations = new MutableLiveData<>();
    public LiveData<List<Station>> stations = _stations;

    public void getStations(@Nullable Integer lineId) {
        AsyncTask.execute(() -> {
            if (lineId == null) {
                _stations.postValue(new ArrayList<>());
            } else {
                _stations.postValue(stationDao.getAllStationsByLineId(lineId));
            }
        });
    }

    private final MutableLiveData<ResultState<AlertCreationResponse>> _creationState = new MutableLiveData<>();
    public LiveData<ResultState<AlertCreationResponse>> creationState = _creationState;

    public void createAlert(Integer cityId, Integer transportId, String comment, Integer lineId, Integer stationId, Integer alertTypeId) {
        AsyncTask.execute(() -> {
            _creationState.postValue(ResultState.loading());
            _creationState.postValue(alertsRepository.createAlert(new AlertCreationRequest(
                    cityId, transportId, comment, lineId, stationId, alertTypeId
            )));
        });
    }

    public LiveData<List<AlertType>> alertTypes = new MutableLiveData<>();

    public void getAlertTypes() {
        alertTypes = alertTypeDao.getAllAlertTypes();
    }

}
