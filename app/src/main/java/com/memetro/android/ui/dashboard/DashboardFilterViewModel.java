package com.memetro.android.ui.dashboard;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.memetro.android.local.dao.models.UIAlert;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class DashboardFilterViewModel extends ViewModel {

    private MutableLiveData<AlertFilter> _alertFilter = new MutableLiveData<>(new AlertFilter());
    public LiveData<AlertFilter> alertFilter = _alertFilter;

    public void updateTransportsFilters(Integer transportId) {
        if(_alertFilter.getValue() != null) {
            AlertFilter filter = _alertFilter.getValue();
            if(filter.transportIds.contains(transportId)) {
                filter.transportIds.remove(transportId);
            } else {
                filter.transportIds.add(transportId);
            }
            _alertFilter.postValue(filter);
        }
    }

    public void updateAlertTypeFilters(Integer alertTypeId) {
        if(_alertFilter.getValue() != null) {
            AlertFilter filter = _alertFilter.getValue();
            if(filter.alertTypeIds.contains(alertTypeId)) {
                filter.alertTypeIds.remove(alertTypeId);
            } else {
                filter.alertTypeIds.add(alertTypeId);
            }
            _alertFilter.postValue(filter);
        }
    }


    public static class AlertFilter {
        public List<Integer> alertTypeIds = new ArrayList<>();
        public List<Integer> transportIds = new ArrayList<>();

        public Boolean isFiltered() {
            return !transportIds.isEmpty()
                    || !alertTypeIds.isEmpty();
        }

        public Boolean alertIsValid(UIAlert alert) {
            boolean isValid = false;
            if(!isFiltered()) {
                return true;
            }
            if((alertTypeIds.isEmpty() || alertTypeIds.contains(alert.getAlert().getAlertTypeId())) &&
                    (transportIds.isEmpty() || transportIds.contains(alert.getAlert().getTransportId()))
            ){
                isValid = true;
            }
            return isValid;
        }
    }

}
