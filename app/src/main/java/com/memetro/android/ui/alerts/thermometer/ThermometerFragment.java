/*
 * Copyright 2013 Nytyr [me at nytyr dot me]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.memetro.android.ui.alerts.thermometer;

import static com.memetro.android.ui.dashboard.DashboardFilterViewModel.*;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;

import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import android.view.View;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.memetro.android.databinding.FragmentThermometerBinding;
import com.memetro.android.api.ResultState;
import com.memetro.android.api.alerts.models.Alert;
import com.memetro.android.api.alerts.models.AlertCreationRequest;
import com.memetro.android.api.banner.models.BannerDataModel;
import com.memetro.android.api.banner.models.BannerResponseModel;
import com.memetro.android.local.dao.models.UIAlert;
import com.memetro.android.ui.BaseFragment;
import com.memetro.android.ui.alerts.addalert.AddAlertFragment;
import com.memetro.android.ui.alerts.thermometer.adapter.BannerViewPagerAdapter;
import com.memetro.android.ui.dashboard.DashboardActivity;
import com.memetro.android.ui.alerts.thermometer.adapter.AlertsAdapter;
import com.memetro.android.ui.dashboard.DashboardFilterViewModel;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class ThermometerFragment extends BaseFragment<FragmentThermometerBinding> {

    private DashboardActivity mActivity;

    Timer timer;

    AlertsAdapter adapter;
    BannerViewPagerAdapter bannerAdapter;
    private ThermometerFragmentViewModel viewModel;
    private DashboardFilterViewModel filterViewModel;
    private BroadcastReceiver broadcastReceiver;

    @Override
    protected void setUpViews(@Nullable Bundle savedInstanceState) {
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.getAction().equals(AddAlertFragment.ALERT_CREATED_EVENT)) {
                    viewModel.getAlertsSync();
                }
            }
        };
        binding.swiperefresh.setOnRefreshListener(() -> {
            viewModel.getAlertsSync();
            viewModel.getBanner();
        });
        adapter = new AlertsAdapter();
        binding.alertsListView.setAdapter(adapter);
        bannerAdapter = new BannerViewPagerAdapter(requireContext());
        binding.bannerPager.setAdapter(bannerAdapter);
    }

    @Override
    protected void onBackPressed() {

    }

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        this.mActivity = (DashboardActivity) getActivity();
    }

    @Override
    public void onPause() {
        super.onPause();
        mActivity.compressActionBar();
        requireActivity().unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        requireActivity().registerReceiver(broadcastReceiver, new IntentFilter(AddAlertFragment.ALERT_CREATED_EVENT));
        mActivity.fullActionBar();
    }

    private void handleBanners(ResultState<List<BannerDataModel>> bannersStatus) {
        if (bannersStatus.getStatus() == ResultState.Status.SUCCESS) {
            List<BannerDataModel> banners = bannersStatus.getData();
            if (banners.isEmpty()) {
                binding.bannerPager.setVisibility(View.GONE);
                if (timer != null) {
                    timer.cancel();
                }
            } else {
                binding.bannerPager.setVisibility(View.VISIBLE);
                bannerAdapter.setItems(banners);
                if (timer != null) {
                    timer.cancel();
                }
                timer = new Timer();
                timer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        mActivity.runOnUiThread(() -> {
                            if (binding.bannerPager.getCurrentItem()< banners.size()-1) {
                                binding.bannerPager.setCurrentItem(binding.bannerPager.getCurrentItem()+1);
                            } else binding.bannerPager.setCurrentItem(0);
                        });
                    }
                }, 5000,5000);
            }
        }
    }

    private void handleAlerts(List<UIAlert> alerts) {
        if(alerts.isEmpty()) {
            binding.noAlerts.setVisibility(View.VISIBLE);
            binding.alertsListView.setVisibility(View.GONE);
        } else {
            binding.noAlerts.setVisibility(View.GONE);
            binding.alertsListView.setVisibility(View.VISIBLE);
            adapter.setItems(alerts);
        }
        if(binding.swiperefresh.isRefreshing()) binding.swiperefresh.setRefreshing(false);
    }

    private void handleFilter(AlertFilter alertFilter) {
        viewModel.filterAlerts(alertFilter);
    }

    @Override
    protected void initViewModels() {
        viewModel = new ViewModelProvider(requireActivity()).get(ThermometerFragmentViewModel.class);
        filterViewModel = new ViewModelProvider(requireActivity()).get(DashboardFilterViewModel.class);
    }

    @Override
    protected FragmentThermometerBinding getBinding() {
        return FragmentThermometerBinding.inflate(getLayoutInflater());
    }

    @Override
    protected void observeViewModel() {
        viewModel.alertsMediatorLiveData.observe(getViewLifecycleOwner(), this::handleAlerts);
        filterViewModel.alertFilter.observe(getViewLifecycleOwner(), this::handleFilter);
        viewModel.bannersLiveData.observe(getViewLifecycleOwner(), this::handleBanners);
    }

}