package com.memetro.android.ui.splash;

import android.os.AsyncTask;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.memetro.android.api.ResultState;
import com.memetro.android.api.login.LoginRepository;
import com.memetro.android.api.login.models.LoginResponseModel;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;

@HiltViewModel
public class SplashScreenViewModel extends ViewModel {

    private final LoginRepository loginRepository;

    @Inject
    public SplashScreenViewModel(LoginRepository loginRepository) {
        this.loginRepository = loginRepository;
    }

    private final MutableLiveData<ResultState<LoginResponseModel>> _autoLoginState = new MutableLiveData<>();
    public LiveData<ResultState<LoginResponseModel>> autoLoginState = _autoLoginState;

    public void autoLogin() {
        AsyncTask.execute(() -> {
            _autoLoginState.postValue(ResultState.loading());
            _autoLoginState.postValue(loginRepository.autoLogin());
        });
    }

    public boolean isAutoLoginEnabled() {
        return loginRepository.isAutoLoginEnabled();
    }
}
