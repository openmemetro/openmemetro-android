/*
 * Copyright 2013 Nytyr [me at nytyr dot me]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.memetro.android.ui.alerts.thermometer.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.memetro.android.databinding.ItemAlertThermometerBinding;
import com.memetro.android.api.alerts.models.Alert;
import com.memetro.android.local.dao.models.UIAlert;

import java.util.Collections;
import java.util.List;

public class AlertsAdapter extends RecyclerView.Adapter<AlertsViewHolder> {

    private List<UIAlert> list_alerts = Collections.emptyList();

    public AlertsAdapter() {
    }

    @NonNull
    @Override
    public AlertsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemAlertThermometerBinding holderView = ItemAlertThermometerBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new AlertsViewHolder(holderView);
    }

    @Override
    public void onBindViewHolder(@NonNull AlertsViewHolder holder, int position) {
        UIAlert alert = list_alerts.get(position);
        holder.bind(alert);
        holder.itemView.setOnClickListener(view -> {
            boolean expanded = alert.getAlert().isExpanded();
            alert.getAlert().setExpanded(!expanded);
            notifyItemChanged(position);
        });
    }

    public void setItems(List<UIAlert> alerts) {
        this.list_alerts = alerts;
        notifyDataSetChanged();
    }

    public void clear() {
        list_alerts.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return list_alerts.size();
    }

}