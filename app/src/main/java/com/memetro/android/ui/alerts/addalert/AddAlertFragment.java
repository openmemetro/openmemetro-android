/*
 * Copyright 2013 Nytyr [me at nytyr dot me]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.memetro.android.ui.alerts.addalert;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.memetro.android.R;
import com.memetro.android.api.sync.models.*;
import com.memetro.android.databinding.FragmentAddAlertBinding;
import com.memetro.android.api.ResultState;
import com.memetro.android.api.alerts.models.AlertCreationResponse;
import com.memetro.android.ui.BaseFragment;
import com.memetro.android.ui.alerts.addalert.adapter.StationSearchAdapter;
import com.memetro.android.ui.dashboard.DashboardActivity;
import com.memetro.android.ui.common.LayoutUtils;
import com.memetro.android.ui.common.MemetroProgress;

import java.util.Collections;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class AddAlertFragment extends BaseFragment<FragmentAddAlertBinding> {

    private MemetroProgress pdialog;
    private AddAlertViewModel viewModel;
    private Integer selectedStationId = null;

    public static final String ALERT_CREATED_EVENT = "ALERT_CREATED_EVENT";

    @Override
    protected void setUpViews(@Nullable Bundle savedInstanceState) {
        this.pdialog = new MemetroProgress(requireActivity());
        ((DashboardActivity) requireActivity()).hideSpeaker();

        // todo: implement more cities
//        binding.spinnerCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                City city = (City) adapterView.getAdapter().getItem(i);
//                viewModel.getTransports(city.getId());
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });

        binding.spinnerTransport.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Transport transport = (Transport) adapterView.getAdapter().getItem(i);
                // Deactivate search station for bus
                if(transport.getName().equals("Bus")) {
                    binding.searchStation.setFocusable(false);
                    binding.searchStation.setFocusableInTouchMode(false);
                    binding.searchStation.setHint(R.string.todas);
                } else {
                    binding.searchStation.setFocusable(true);
                    binding.searchStation.setFocusableInTouchMode(true);
                    binding.searchStation.setHint(R.string.autocompletar);
                }
                viewModel.getLines(transport.getId());
                binding.searchStation.setText("");
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        binding.spinnerLine.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Line line = (Line) adapterView.getAdapter().getItem(i);
                binding.searchStation.setText("");
                viewModel.getStations(line.getId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        StationSearchAdapter stationAdapter = new StationSearchAdapter(
                requireContext(),
                android.R.layout.simple_list_item_1,
                Collections.emptyList()
        );

        binding.searchStation.setAdapter(stationAdapter);
        binding.searchStation.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                selectedStationId = null;
                stationAdapter.getFilter().filter(s.toString());
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
        });
        binding.searchStation.setOnItemClickListener((parent, arg1, pos, id) ->
                selectedStationId = ((Station) binding.searchStation.getAdapter().getItem(pos)).getId()
        );


        binding.addAlertButton.setOnClickListener(view -> {
            if (((Transport) binding.spinnerTransport.getSelectedItem()).getName().equals("Bus"))
                selectedStationId = ((Station) binding.searchStation.getAdapter().getItem(0)).getId();
            if(selectedStationId != null) {
                viewModel.createAlert(
//                        ((City) binding.spinnerCity.getSelectedItem()).getId(),
                        viewModel.cities.getValue().get(0).getId(),
                        ((Transport) binding.spinnerTransport.getSelectedItem()).getId(),
                        binding.descriptionText.getText().toString(),
                        ((Line) binding.spinnerLine.getSelectedItem()).getId(),
                        //((Station) binding.spinnerStation.getSelectedItem()).getId()
                        selectedStationId,
                        ((AlertType)binding.spinnerAlertType.getSelectedItem()).getId()
                );
            } else {
                Toast.makeText(requireContext(), "Selecciona una estación pulsando en ella", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void handleAlertCreation(ResultState<AlertCreationResponse> creationState) {
        switch (creationState.getStatus()){
            case SUCCESS:
                pdialog.hide();
                requireContext().sendBroadcast(new Intent(ALERT_CREATED_EVENT));
                getParentFragmentManager().popBackStackImmediate();
                break;
            case ERROR:
                pdialog.hide();
                break;
            case LOADING:
                pdialog.show();
                break;
        }
    }

    @Override
    protected void onBackPressed() { }

    @Override
    public void onDestroy() {
        ((DashboardActivity) requireActivity()).showSpeaker();
        super.onDestroy();
    }

    @Override
    protected FragmentAddAlertBinding getBinding() {
        return FragmentAddAlertBinding.inflate(getLayoutInflater());
    }

    @Override
    protected void initViewModels() {
        viewModel = new ViewModelProvider(this).get(AddAlertViewModel.class);
    }

    @Override
    protected void observeViewModel() {
        viewModel.cities.observe(getViewLifecycleOwner(), cities -> {
            if(cities != null) {
//                LayoutUtils.setDefaultSpinner(
//                        requireActivity(),
//                        binding.spinnerCity,
//                        cities
//                );
                if(cities.size()>0) viewModel.getTransports(cities.get(0).getId());
            }
        });
        viewModel.transport.observe(getViewLifecycleOwner(), transports -> {
            if(transports != null) {
                LayoutUtils.setDefaultSpinner(
                        requireActivity(),
                        binding.spinnerTransport,
                        transports
                );
                if(transports.size()>0)  viewModel.getLines(transports.get(0).getId());
            }
        });
        viewModel.lines.observe(getViewLifecycleOwner(), lines -> {
            if(lines!=null) {
                LayoutUtils.setDefaultSpinner(
                        requireActivity(),
                        binding.spinnerLine,
                        lines
                );

                binding.searchStation.setEnabled(lines.size()>0);
            }
        });
        viewModel.stations.observe(getViewLifecycleOwner(), stations -> {
            if(stations != null) {
                binding.searchStation.setAdapter(
                        new StationSearchAdapter(requireContext(), android.R.layout.select_dialog_item, stations)
                );
            }
        });
        viewModel.creationState.observe(getViewLifecycleOwner(), this::handleAlertCreation);

        viewModel.alertTypes.observe(getViewLifecycleOwner(), alertTypes -> {
            if(alertTypes != null) {
                LayoutUtils.setDefaultSpinner(
                        requireActivity(),
                        binding.spinnerAlertType,
                        alertTypes
                );
            }
        });
    }
}