package com.memetro.android.ui.dashboard.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.memetro.android.databinding.FilterTransportItemBinding;
import com.memetro.android.api.sync.models.Transport;

import java.util.Collections;
import java.util.List;


public class FilterTransportAdapter extends RecyclerView.Adapter<FilterTransportViewHolder> {

    private List<Transport> list_transports = Collections.emptyList();
    private TransportFilterCallback callback;

    public FilterTransportAdapter(TransportFilterCallback callback) {
        this.callback = callback;
    }

    @NonNull
    @Override
    public FilterTransportViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        FilterTransportItemBinding holderView = FilterTransportItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new FilterTransportViewHolder(holderView);
    }

    @Override
    public void onBindViewHolder(@NonNull FilterTransportViewHolder holder, int position) {
        holder.bind(list_transports.get(position), callback);
    }

    public void setItems(List<Transport> transports) {
        this.list_transports = transports;
        notifyDataSetChanged();
    }

    public void clear() {
        list_transports.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return list_transports.size();
    }

    public interface TransportFilterCallback {
        void onTransportSelected(Integer transportId);
    }
}
