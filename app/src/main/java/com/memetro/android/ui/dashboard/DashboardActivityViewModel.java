package com.memetro.android.ui.dashboard;

import android.os.AsyncTask;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.memetro.android.api.ResultState;
import com.memetro.android.api.sync.SyncRepository;
import com.memetro.android.api.sync.models.AlertType;
import com.memetro.android.api.sync.models.SyncResponseModel;
import com.memetro.android.api.sync.models.Transport;
import com.memetro.android.api.sync.models.User;
import com.memetro.android.api.user.UserRepository;

import java.util.List;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;

@HiltViewModel
public class DashboardActivityViewModel extends ViewModel {

    private final SyncRepository syncRepository;
    private final UserRepository userRepository;

    @Inject
    public DashboardActivityViewModel(SyncRepository syncRepository, UserRepository userRepository) {
        this.syncRepository = syncRepository;
        this.userRepository = userRepository;
        sync();
    }

    public User getUser() {
        return userRepository.getUser();
    }

    private final MutableLiveData<ResultState<SyncResponseModel>> _syncState = new MutableLiveData<>();
    public LiveData<ResultState<SyncResponseModel>> syncSate = _syncState;

    public void sync() {
        _syncState.setValue(ResultState.loading());
        AsyncTask.execute(() -> _syncState.postValue(syncRepository.sync()));
    }

    public LiveData<List<Transport>> getTransports() {
        return syncRepository.getTransports();
    }

    public LiveData<List<AlertType>> getAlertTypes() {
        return syncRepository.getAlertTypes();
    }

    public void logout() {
        userRepository.logout();
    }
}
