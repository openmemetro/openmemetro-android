/*
 * Copyright 2013 Nytyr [me at nytyr dot me]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.memetro.android.ui.dashboard;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;

import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.memetro.android.R;
import com.memetro.android.api.sync.models.AlertType;
import com.memetro.android.databinding.ActivityDashboardBinding;
import com.memetro.android.databinding.LeftmenuBinding;
import com.memetro.android.databinding.RightMenuBinding;
import com.memetro.android.api.ResultState;
import com.memetro.android.api.sync.models.SyncResponseModel;
import com.memetro.android.api.sync.models.Transport;
import com.memetro.android.api.sync.models.User;
import com.memetro.android.notifications.AlertForegroundService;
import com.memetro.android.ui.BaseActivity;
import com.memetro.android.ui.alerts.addalert.AddAlertFragment;
import com.memetro.android.ui.alerts.thermometer.ThermometerFragment;
import com.memetro.android.ui.common.MemetroDialog;
import com.memetro.android.ui.common.MemetroProgress;
import com.memetro.android.ui.dashboard.adapter.FilterAlertTypeAdapter;
import com.memetro.android.ui.dashboard.adapter.FilterTransportAdapter;
import com.memetro.android.ui.login.LoginActivity;
import com.memetro.android.ui.settings.SettingsFragment;

import java.util.List;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class DashboardActivity extends BaseActivity<ActivityDashboardBinding> {

    private Boolean lastWindow = true;
    private MemetroProgress pdialog;

    private DashboardActivityViewModel viewModel;
    private DashboardFilterViewModel filterViewModel;

    private LeftmenuBinding menuHeaderBinding;
    private RightMenuBinding rightMenuBinding;

    private FilterTransportAdapter filterTransportAdapter;
    private FilterAlertTypeAdapter filterAlertTypeAdapter;

    @Override
    protected void setUpViews(@Nullable Bundle savedInstanceState) {
        pdialog = new MemetroProgress(this);
        // Launch default fragment
        changeMainFragment(new ThermometerFragment(), true);

        setRightMenu();
        setLeftMenu();

        // Speaker Button
        binding.speakerButton.setOnClickListener(view ->
                changeAddModelFragment(new AddAlertFragment())
        );

        // Toggler listener
        setActionBar(binding.actionbar.toolbar);
    }

    private void openUrlWeb(String url) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    private void logout() {
        viewModel.logout();
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void setRightMenu() {
        // Filter
        binding.actionbar.actionbarFilter.setOnClickListener((view) -> {
            if (lastWindow) {
                binding.drawerLayout.openDrawer(Gravity.RIGHT);
            } else {
                onBackPressed();
            }
        });
        rightMenuBinding = RightMenuBinding.inflate(getLayoutInflater());
        binding.rightMenu.addHeaderView(rightMenuBinding.getRoot());
        filterTransportAdapter = new FilterTransportAdapter((transportId) ->
                filterViewModel.updateTransportsFilters(transportId)
        );
        filterAlertTypeAdapter = new FilterAlertTypeAdapter((alertTypeId) ->
                        filterViewModel.updateAlertTypeFilters(alertTypeId)
                );
        rightMenuBinding.alertTypeRv.setAdapter(filterAlertTypeAdapter);
        rightMenuBinding.transportRv.setAdapter(filterTransportAdapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rightMenuBinding.transportRv.getContext(),
                LinearLayout.VERTICAL);
        rightMenuBinding.transportRv.addItemDecoration(dividerItemDecoration);
        rightMenuBinding.alertTypeRv.addItemDecoration(dividerItemDecoration);
    }

    private void setTransportsToRightMenu(List<Transport> transports) {
        filterTransportAdapter.setItems(transports);
    }

    private void setAlertTypesToRightMenu(List<AlertType> alertTypes) {
        filterAlertTypeAdapter.setItems(alertTypes);
    }

    private void setLeftMenu() {
        binding.actionbar.actionbarToggler.setOnClickListener((view) -> {
            if (lastWindow) {
                binding.drawerLayout.openDrawer(Gravity.LEFT);
            } else {
                onBackPressed();
            }
        });
        menuHeaderBinding = LeftmenuBinding.inflate(getLayoutInflater());
        binding.leftMenu.addHeaderView(menuHeaderBinding.getRoot());
        /// Menu buttons ///
        binding.leftMenu.setNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.alertsMenu:
                    binding.drawerLayout.openDrawer(Gravity.LEFT);
                    changeMainFragment(new ThermometerFragment(), true);
                    return true;
                case R.id.memetroMenu:
                    openUrlWeb("http://www.memetro.net/trastorno-memetro/");
                    return true;
                /*case R.id.configMenu:
                    binding.drawerLayout.openDrawer(Gravity.LEFT);
                    changeAddModelFragment(new SettingsFragment());
                    break;*/
                case R.id.infoMenu:
                    openUrlWeb("https://memetro.org");
                    return true;
                case R.id.logoutMenu:
                    logout();
                    return true;
            }
            return false;
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        Intent serviceIntent = new Intent(this, AlertForegroundService.class);
        serviceIntent.putExtra("inputExtra", "Foreground Service Example in Android");
        ContextCompat.startForegroundService(this, serviceIntent);
    }

    @Override
    public void onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(Gravity.LEFT)) {
            binding.drawerLayout.closeDrawer(Gravity.LEFT);
            return;
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.getBackStackEntryCount() > 0) {
            super.onBackPressed();
            return;
        }

        if (!lastWindow) {
            changeMainFragment(new ThermometerFragment(), true);
            return;
        }

        super.onBackPressed();
    }

    public void showSpeaker() {
        binding.speakerButton.setVisibility(View.VISIBLE);
    }

    public void hideSpeaker() {
        binding.speakerButton.setVisibility(View.GONE);
    }

    public void disableSliderMenu() {
        //binding.slideHolder.setEnabled(false);
    }

    public void activateSliderMenu() {
        //binding.slideHolder.setEnabled(true);
    }

    public void fullActionBar() {
        //binding.actionbar.actionbarTws.setVisibility(View.VISIBLE);
        //binding.actionbar.actionbarMap.setVisibility(View.VISIBLE);

    }

    public void compressActionBar() {
        //binding.actionbar.actionbarTws.setVisibility(View.GONE);
        //binding.actionbar.actionbarMap.setVisibility(View.GONE);
    }

    public void changeMainFragment(Fragment fragment) {
        changeMainFragment(fragment, false);
    }

    private void changeMainFragment(Fragment fragment, Boolean isLastWindow) {
        lastWindow = isLastWindow;
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragmentData, fragment).commit();

        // Toggle left menu
        if (binding.drawerLayout.isDrawerOpen(Gravity.LEFT)) {
            binding.drawerLayout.closeDrawer(Gravity.LEFT);
        }
    }

    private void changeAddModelFragment(Fragment fragment) {
        lastWindow = false;
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().add(R.id.fragmentData, fragment)
                .addToBackStack(fragment.getClass().toString())
                .commit();

        // Toggle left menu
        if (binding.drawerLayout.isDrawerOpen(Gravity.LEFT)) {
            binding.drawerLayout.closeDrawer(Gravity.LEFT);
        }
    }


    private void handleSyncState(ResultState<SyncResponseModel> syncState) {
        switch (syncState.getStatus()) {
            case SUCCESS:
                if (pdialog.isShowing()) pdialog.dismiss();
                User user = syncState.getData().getData().getUser();
                if(user.getImgUrl() != null) {
                    Glide.with(this)
                            .load(user.getImgUrl())
                            .circleCrop()
                            .into(menuHeaderBinding.avatarMenu);
                }
                menuHeaderBinding.usernameMenu.setText("@" + user.getUsername());
                break;
            case ERROR:
                MemetroDialog.showDialog(this, null, getString(R.string.sync_error));
                break;
            case LOADING:
                pdialog.show();
                break;
        }
    }

    @Override
    protected ActivityDashboardBinding getBinding() {
        return ActivityDashboardBinding.inflate(getLayoutInflater());
    }

    @Override
    protected void initViewModels() {
        viewModel = new ViewModelProvider(this).get(DashboardActivityViewModel.class);
        filterViewModel = new ViewModelProvider(this).get(DashboardFilterViewModel.class);
    }

    @Override
    protected void observeViewModel() {
        viewModel.syncSate.observe(this, this::handleSyncState);
        viewModel.getTransports().observe(this, this::setTransportsToRightMenu);
        viewModel.getAlertTypes().observe(this, this::setAlertTypesToRightMenu);
    }

}
