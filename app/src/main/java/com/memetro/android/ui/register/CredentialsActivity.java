/*
 * Copyright 2013 Josei [hi at jrubio dot me]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.memetro.android.ui.register;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.View;
import android.widget.*;

import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import com.bumptech.glide.Glide;
import com.memetro.android.R;
import com.memetro.android.databinding.ActivityRegisterCredentialsBinding;
import com.memetro.android.api.ResultState;
import com.memetro.android.api.captcha.models.CaptchaDataModel;
import com.memetro.android.api.signup.models.SignupResponseModel;
import com.memetro.android.api.sync.models.City;
import com.memetro.android.api.sync.models.Country;
import com.memetro.android.api.sync.models.StaticDataResponseModel;
import com.memetro.android.sharedprefs.SharedPrefs;
import com.memetro.android.ui.BaseActivity;
import com.memetro.android.ui.common.LayoutUtils;

import com.memetro.android.ui.common.MemetroDialog;
import com.memetro.android.ui.common.MemetroProgress;
import com.memetro.android.utils.DeviceUuidFactory;
import dagger.hilt.android.AndroidEntryPoint;

import javax.inject.Inject;
import java.util.Objects;

import static com.memetro.android.ui.register.CredentialsViewModel.PASS_MIN_LENGTH;

@AndroidEntryPoint
public class CredentialsActivity extends BaseActivity<ActivityRegisterCredentialsBinding> {

    @Inject
    SharedPrefs sharedPrefs;

    public static String SIGNUP_USERNAME_EXTRA = "SIGNUP_USERNAME_EXTRA";
    public static String SIGNUP_PASS_EXTRA = "SIGNUP_PASS_EXTRA";

    private static String TAG = "Memetro Register Credentials";
    private CredentialsViewModel viewModel;
    private MemetroProgress pdialog;

    private City city;
    private Country country;

    private String captchaKey;

    @Override
    protected void setUpViews(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        pdialog = new MemetroProgress(this);
        _usernameCheck();
        _passwordCheck();

        binding.register.setOnClickListener(view -> {
            if(!Objects.equals(viewModel.passwordState.getValue(), CredentialsViewModel.PasswordStatus.OK)
                    || !Objects.equals(viewModel.userNameState.getValue(), CredentialsViewModel.UserNameStatus.OK) ) {
                String message = view.getRootView().getContext(). getString(R.string.error_register);
                if(binding.passwordStatusMessage.getText().toString().length() > 0) message = message + " \n" + binding.passwordStatusMessage.getText().toString();
                if(!(binding.usernameStatusMessage.getText().toString().isEmpty())) message = message + " \n" + binding.usernameStatusMessage.getText().toString();
                MemetroDialog.showDialog(CredentialsActivity.this, null, message);
            }
            else if(city == null || country == null){
                String message = view.getRootView().getContext(). getString(R.string.error_register_city_country);
                MemetroDialog.showDialog(CredentialsActivity.this, null, message);
            }
            else if(binding.solveCaptcha.getText().toString().isEmpty()){
                String message = view.getRootView().getContext(). getString(R.string.error_register_resolve_captcha);
                MemetroDialog.showDialog(CredentialsActivity.this, null, message);
            }
            else {
                DeviceUuidFactory uuidFactory = new DeviceUuidFactory(getApplicationContext(), sharedPrefs);
                viewModel.register(
                        binding.username.getText().toString(),
                        binding.password.getText().toString(),
                        country.getId(),
                        city.getId(),
                        binding.solveCaptcha.getText().toString(),
                        captchaKey,
                        uuidFactory.getDeviceUuid().toString()
                );
            }
        });
        binding.spinnerCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                country = (Country) adapterView.getAdapter().getItem(i);
                viewModel.getCities(country.getId());
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        binding.spinnerCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                city = (City) adapterView.getAdapter().getItem(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        binding.username.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                _usernameCheck();
            }
            @Override
            public void afterTextChanged(Editable editable) {}
        });

        binding.password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                _passwordCheck();
            }
            @Override
            public void afterTextChanged(Editable editable) {}
        });

        binding.repeatPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                _passwordCheck();
            }
            @Override
            public void afterTextChanged(Editable editable) {}
        });

        binding.captchaReload.setOnClickListener(view -> {
           viewModel.getCaptcha();
        });
    }

    private void _usernameCheck() {
        viewModel.usernameCheck(binding.username.getText().toString());
    }

    private void _passwordCheck() {
        viewModel.passwordCheck(binding.password.getText().toString(), binding.repeatPassword.getText().toString());
    }

    protected void handleSignupState(ResultState<SignupResponseModel> signupState) {
        switch (signupState.getStatus()) {
            case SUCCESS:
                if (pdialog.isShowing()) pdialog.dismiss();
                Toast.makeText(context, getString(R.string.register_ok), Toast.LENGTH_SHORT).show();
                Bundle data = new Bundle();
                data.putString(SIGNUP_PASS_EXTRA, binding.password.getText().toString());
                data.putString(SIGNUP_USERNAME_EXTRA, binding.username.getText().toString());
                Intent resultData = new Intent();
                resultData.putExtras(data);
                this.setResult(Activity.RESULT_OK, resultData);
                finish();
                break;
            case ERROR:
                if (pdialog.isShowing()) pdialog.dismiss();
                viewModel.getCaptcha();
                String message = "Error en el registro. El nombre de usuario podría estar en uso!";
                if (signupState.getCode() == 409) message = "Error en el Captcha, intentalo de nuevo!";
                MemetroDialog.showDialog(CredentialsActivity.this, null, message);
                break;
            case LOADING:
                if (!pdialog.isShowing()) pdialog.show();
                break;
        }
    }

    protected void handleUserNameState(CredentialsViewModel.UserNameStatus status) {
        switch (status) {
            case OK:
                binding.register.setEnabled(true);
                binding.usernameStatusMessage.setText("");
                break;
            case EMPTY:
                binding.usernameStatusMessage.setText("El nombre de usuario no puede estar vacío");
                break;
            case NOT_ALNUM:
                binding.usernameStatusMessage.setText("El nombre de usuario solo puede contener caracteres de la A a la Z y números ");
                break;
            default:
                binding.usernameStatusMessage.setText("");
                break;
        }
    }

    protected void handlePasswordState(CredentialsViewModel.PasswordStatus status) {
        switch (status) {
            case OK:
                binding.register.setEnabled(true);
                binding.passwordStatusMessage.setText("");
                break;
            case EMPTY:
                binding.passwordStatusMessage.setText("La contraseña no puede estar vacía ");
                break;
            case TOO_SHORT:
                binding.passwordStatusMessage.setText("Contraseña tiene que tener mínimo " + String.valueOf(PASS_MIN_LENGTH));
                break;
            case NOT_MATCH:
                binding.passwordStatusMessage.setText("Las contraseñas no coinciden!");
                break;
            case NOT_NUMS:
                binding.passwordStatusMessage.setText("La contraseña tiene que contener números!");
                break;
            default:
                binding.passwordStatusMessage.setText("");
                break;
        }
    }

    private void handleStaticDataState(ResultState<StaticDataResponseModel> result){
        switch (result.getStatus()) {
            case SUCCESS:
                if (pdialog.isShowing()) pdialog.dismiss();
                break;
            case ERROR:
                if (pdialog.isShowing()) pdialog.dismiss();
                MemetroDialog.showDialog(CredentialsActivity.this, null, getString(R.string.reach_server_error), this::finish);
                break;
            case LOADING:
                pdialog.show();
                break;
        }
    }

    private void handleCaptchaState(ResultState<CaptchaDataModel> result){
        switch (result.getStatus()) {
            case SUCCESS:
                if (pdialog.isShowing()) pdialog.dismiss();
                byte[] imageByteArray = Base64.decode(result.getData().getCaptchaImage(), Base64.DEFAULT);
                Glide.with(this)
                        .asBitmap()
                        .load(imageByteArray)
                        .into(binding.captchaImage);
                this.captchaKey = result.getData().getCaptchaKey();
                break;
            case ERROR:
                if (pdialog.isShowing()) pdialog.dismiss();
                MemetroDialog.showDialog(CredentialsActivity.this, null, getString(R.string.reach_captcha_error));
                break;
            case LOADING:
                pdialog.show();
                break;
        }
    }

    @Override
    protected ActivityRegisterCredentialsBinding getBinding() {
        return ActivityRegisterCredentialsBinding.inflate(getLayoutInflater());
    }

    @Override
    protected void observeViewModel() {
        viewModel.countries.observe(this, countries -> {
            if(countries != null) {
                LayoutUtils.setDefaultSpinner(
                        this,
                        binding.spinnerCountry,
                        countries
                );
                if(countries.size()>0) viewModel.getCities(countries.get(0).getId());
            }
        });
        viewModel.cities.observe(this, cities -> {
            if(cities != null) {
                LayoutUtils.setDefaultSpinner(
                        this,
                        binding.spinnerCity,
                        cities
                );
            }
        });
        viewModel.signupState.observe(this, this::handleSignupState);
        viewModel.userNameState.observe(this, this::handleUserNameState);
        viewModel.passwordState.observe(this, this::handlePasswordState);
        viewModel.staticDataState.observe(this, this::handleStaticDataState);
        viewModel.captchaState.observe(this, this::handleCaptchaState);
    }

    @Override
    protected void initViewModels() {
        viewModel = new ViewModelProvider(this).get(CredentialsViewModel.class);
    }

}
