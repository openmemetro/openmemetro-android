/*
 * Copyright 2013 Nytyr [me at nytyr dot me]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.memetro.android.ui.login;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.memetro.android.R;
import com.memetro.android.databinding.ActivityMainBinding;
import com.memetro.android.api.ResultState;
import com.memetro.android.api.login.models.LoginResponseModel;
import com.memetro.android.sharedprefs.SharedPrefs;
import com.memetro.android.ui.BaseActivity;
import com.memetro.android.ui.dashboard.DashboardActivity;
import com.memetro.android.ui.common.MemetroDialog;
import com.memetro.android.ui.common.MemetroProgress;
import com.memetro.android.ui.register.CredentialsActivity;


import javax.inject.Inject;

import com.memetro.android.utils.DeviceUuidFactory;
import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class LoginActivity extends BaseActivity<ActivityMainBinding> {

    @Inject
    SharedPrefs sharedPrefs;

    private MemetroProgress pdialog;

    private LoginActivityViewModel viewModel;

    @Override
    protected void setUpViews(@Nullable Bundle savedInstanceState) {
        pdialog = new MemetroProgress(this);

//        binding.recoverPassButton.setOnClickListener(view -> showRecoverPassDialog(LoginActivity.this));

        binding.register.setOnClickListener(view -> {
            startSignup.launch(
                    new Intent(context, CredentialsActivity.class)
            );
            //Intent intent = new Intent(context, CredentialsActivity.class);
            //startActivity(intent);
        });

        binding.login.setOnClickListener(view -> {
            DeviceUuidFactory uuidFactory = new DeviceUuidFactory(getApplicationContext(), sharedPrefs);
            viewModel.login(
                    binding.username.getText().toString(),
                    binding.password.getText().toString(),
                    uuidFactory.getDeviceUuid().toString()
            );
        });
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    private void handleLoginState(ResultState<LoginResponseModel> loginState) {
        switch (loginState.getStatus()) {
            case SUCCESS:
                if (pdialog.isShowing()) {
                    pdialog.dismiss();
                    viewModel.setAutologin(binding.autologin.isChecked());
                    runOnUiThread(() -> {
                        Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
                        startActivity(intent);
                        finish();
                    });
                }
                break;
            case ERROR:
                if (pdialog.isShowing()) pdialog.dismiss();
                MemetroDialog.showDialog(LoginActivity.this, null, getString(R.string.login_error));
                break;
            case LOADING:
                pdialog.show();
                break;
        }
    }

    public void showRecoverPassDialog(final Context context) {

        final Dialog mDialog = new Dialog(context);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(R.layout.dialog_recover);
        mDialog.setCancelable(true);

//        final EditText emailText = (EditText) mDialog.findViewById(R.id.email);
//        Button sendButton = (Button) mDialog.findViewById(R.id.send);

//        sendButton.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                // TODO
//                /*RecoverPassUtils.recoverPass(context, emailText.getText().toString(), new OAuthHandler() {
//                    public void onStart() {
//                        pdialog.show();
//                    }
//
//                    public void onSuccess() {
//                        mDialog.dismiss();
//                        MemetroDialog.showDialog(LoginActivity.this, null, getString(R.string.recover_ok));
//                    }
//
//                    public void onFailure() {
//                        MemetroDialog.showDialog(LoginActivity.this, null, getString(R.string.recover_ko));
//                    }
//
//                    public void onFinish() {
//                        pdialog.dismiss();
//                    }
//                });*/
//            }
//        });

        mDialog.show();
    }


    @Override
    protected ActivityMainBinding getBinding() {
        return ActivityMainBinding.inflate(getLayoutInflater());
    }

    @Override
    protected void initViewModels() {
        viewModel = new ViewModelProvider(this).get(LoginActivityViewModel.class);
    }

    @Override
    protected void observeViewModel() {
        viewModel.loginSate.observe(this, this::handleLoginState);
    }

    ActivityResultLauncher<Intent> startSignup = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if(result.getResultCode() == Activity.RESULT_OK) {
                        if(result.getData() != null) {
                            String username = result.getData().getStringExtra("SIGNUP_USERNAME_EXTRA");
                            String password = result.getData().getStringExtra("SIGNUP_PASS_EXTRA");
                            DeviceUuidFactory uuidFactory = new DeviceUuidFactory(getApplicationContext(), sharedPrefs);
                            viewModel.login(
                                    username,
                                    password,
                                    uuidFactory.getDeviceUuid().toString()
                            );
                        }
                    }
                }
            });

}
