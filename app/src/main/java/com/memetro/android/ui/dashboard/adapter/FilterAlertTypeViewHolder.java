package com.memetro.android.ui.dashboard.adapter;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.memetro.android.api.sync.models.AlertType;
import com.memetro.android.api.sync.models.Transport;
import com.memetro.android.databinding.FilterAlertTypeItemBinding;
import com.memetro.android.databinding.FilterTransportItemBinding;

public class FilterAlertTypeViewHolder extends RecyclerView.ViewHolder {

    private FilterAlertTypeItemBinding itemBinding;

    public FilterAlertTypeViewHolder(FilterAlertTypeItemBinding holderView) {
        super(holderView.getRoot());
        this.itemBinding = holderView;
    }

    public void bind(AlertType alertType, FilterAlertTypeAdapter.AlertTypeFilterCallback callback) {
        itemBinding.transportLayout.setOnClickListener((view) -> {
            itemBinding.alertTypeCheckBox.setChecked(!itemBinding.alertTypeCheckBox.isChecked());
            callback.onAlertTypeSelected(alertType.getId());
        });
        itemBinding.alertTypeName.setText(alertType.getName());

        itemBinding.alertTypeIcon.setText(alertType.getColor());
    }

}
