package com.memetro.android.ui.dashboard.adapter;

import android.graphics.drawable.Drawable;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.memetro.android.R;
import com.memetro.android.databinding.FilterTransportItemBinding;
import com.memetro.android.api.sync.models.Transport;

public class FilterTransportViewHolder extends RecyclerView.ViewHolder {

    private FilterTransportItemBinding itemBinding;

    public FilterTransportViewHolder(FilterTransportItemBinding holderView) {
        super(holderView.getRoot());
        this.itemBinding = holderView;
    }

    public void bind(Transport transport, FilterTransportAdapter.TransportFilterCallback callback) {
        itemBinding.transportLayout.setOnClickListener((view) -> {
            itemBinding.transportCheckbox.setChecked(!itemBinding.transportCheckbox.isChecked());
            callback.onTransportSelected(transport.getId());
        });
        itemBinding.transportName.setText(transport.getName());

        itemBinding.transportIcon.setImageDrawable(
                ContextCompat.getDrawable(itemBinding.getRoot().getContext(), transport.getIcon().iconRef)
        );
    }

}
