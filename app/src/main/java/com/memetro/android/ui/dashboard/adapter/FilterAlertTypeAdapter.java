package com.memetro.android.ui.dashboard.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.memetro.android.api.sync.models.AlertType;
import com.memetro.android.databinding.FilterAlertTypeItemBinding;

import java.util.ArrayList;
import java.util.List;


public class FilterAlertTypeAdapter extends RecyclerView.Adapter<FilterAlertTypeViewHolder> {

    private List<AlertType> list_alert_types = new ArrayList<AlertType>();
    private AlertTypeFilterCallback callback;

    public FilterAlertTypeAdapter(AlertTypeFilterCallback callback) {
        this.callback = callback;
    }

    @NonNull
    @Override
    public FilterAlertTypeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        FilterAlertTypeItemBinding holderView = FilterAlertTypeItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new FilterAlertTypeViewHolder(holderView);
    }

    @Override
    public void onBindViewHolder(@NonNull FilterAlertTypeViewHolder holder, int position) {
        holder.bind(list_alert_types.get(position), callback);
    }

    public void setItems(List<AlertType> transports) {
        this.list_alert_types = transports;
        notifyDataSetChanged();
    }

    public void clear() {
        list_alert_types.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return list_alert_types.size();
    }

    public interface AlertTypeFilterCallback {
        void onAlertTypeSelected(Integer alertTypeId);
    }
}
