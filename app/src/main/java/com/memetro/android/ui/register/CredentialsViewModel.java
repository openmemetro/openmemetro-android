package com.memetro.android.ui.register;

import android.os.AsyncTask;
import android.util.Log;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.memetro.android.BuildConfig;
import com.memetro.android.api.ResultState;
import com.memetro.android.api.captcha.CaptchaRepository;
import com.memetro.android.api.captcha.models.CaptchaDataModel;
import com.memetro.android.api.login.models.LoginRequestModel;
import com.memetro.android.api.login.models.LoginResponseModel;
import com.memetro.android.api.signup.SignupRepository;
import com.memetro.android.api.signup.models.SignupRequestModel;
import com.memetro.android.api.signup.models.SignupResponseModel;
import com.memetro.android.api.sync.StaticDataRepository;
import com.memetro.android.api.sync.models.City;
import com.memetro.android.api.sync.models.Country;
import com.memetro.android.api.sync.models.StaticDataResponseModel;
import com.memetro.android.local.dao.*;
import dagger.hilt.android.lifecycle.HiltViewModel;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@HiltViewModel
public class CredentialsViewModel extends ViewModel {

    private final CountryDao countryDao;
    private final CityDao cityDao;
    private final SignupRepository signupRepository;

    private final StaticDataRepository staticDataRepository;

    private final CaptchaRepository captchaRepository;

    @Inject
    public CredentialsViewModel(CountryDao countryDao, CityDao cityDao, SignupRepository signupRepository,
                                StaticDataRepository staticDataRepository, CaptchaRepository captchaRepository) {
        this.cityDao = cityDao;
        this.countryDao = countryDao;
        this.signupRepository = signupRepository;
        this.staticDataRepository = staticDataRepository;
        this.captchaRepository = captchaRepository;
        getStaticData();
        getCountries();
        getCaptcha();
    }

    private final MutableLiveData<ResultState<StaticDataResponseModel>> _staticDataState = new MutableLiveData<>();
    public LiveData<ResultState<StaticDataResponseModel>> staticDataState = _staticDataState;

    public void getStaticData() {
        AsyncTask.execute(() -> {
            _staticDataState.postValue(ResultState.loading());
            _staticDataState.postValue(staticDataRepository.sync());
        });
    }

    private final MutableLiveData<ResultState<SignupResponseModel>> _signupState = new MutableLiveData<>();
    public LiveData<ResultState<SignupResponseModel>> signupState = _signupState;

    public void register(
            String username,
            String password,
            int countryId,
            int cityId,
            String captchaValue,
            String captchaKey,
            String deviceId
    ) {
        SignupRequestModel request = new SignupRequestModel();
        request.setUsername(username);
        request.setPassword(password);
        request.setCityId(cityId);
        request.setCountryId(countryId);
        request.setCaptchaValue(captchaValue);
        request.setCaptchaKey(captchaKey);
        request.setDeviceId(deviceId);
        AsyncTask.execute(() -> {
            _signupState.postValue(ResultState.loading());
            ResultState<SignupResponseModel> result =
                    signupRepository.register(
                            request
                    );
            _signupState.postValue(result);
        });
    }

    public LiveData<List<Country>> countries = new MutableLiveData<>();

    public void getCountries() {
        countries = countryDao.getAllCountries();
    }

    MutableLiveData<List<City>> _cities = new MutableLiveData<>();
    public LiveData<List<City>> cities = _cities;

    public void getCities(@Nullable Integer countryId) {
        AsyncTask.execute(() -> {
            if (countryId == null) {
                _cities.postValue(new ArrayList<>());
            } else {
                _cities.postValue(cityDao.getCitiesByCountryId(countryId));
            }
        });
    }

    public enum PasswordStatus {
        OK, EMPTY, NOT_MATCH, TOO_SHORT, NOT_NUMS
    }

    public static final int PASS_MIN_LENGTH = 8;

    MutableLiveData<PasswordStatus> _passwordState = new MutableLiveData<>();
    public LiveData<PasswordStatus> passwordState = _passwordState;

    public void passwordCheck(String pass, String repeatPass) {
        if (pass.isEmpty()) {
            _passwordState.postValue(PasswordStatus.EMPTY);
        } else if (!pass.equals(repeatPass)) {
            _passwordState.postValue(PasswordStatus.NOT_MATCH);
//        } else if (pass.length() < PASS_MIN_LENGTH) {
//            _passwordState.postValue(PasswordStatus.TOO_SHORT);
//        } else if (!pass.matches(".*\\d+.*")) {
//            _passwordState.postValue(PasswordStatus.NOT_NUMS);
        } else {
            _passwordState.postValue(PasswordStatus.OK);
        }
    }

    public enum UserNameStatus {
        OK, EMPTY, NOT_ALNUM
    }

    MutableLiveData<UserNameStatus> _usernameState = new MutableLiveData<>();
    public LiveData<UserNameStatus> userNameState = _usernameState;

    public void usernameCheck(String username) {
        if (username.isEmpty()) {
            _usernameState.postValue(UserNameStatus.EMPTY);
        } else if (!username.matches("[A-Za-z0-9]+")) {
            _usernameState.postValue(UserNameStatus.NOT_ALNUM);
        } else {
            _usernameState.postValue(UserNameStatus.OK);
        }
    }

    // OLD MAIL IMPLEMENTATION
    MutableLiveData<Boolean> _emailStatus = new MutableLiveData<>();
    public LiveData<Boolean> emailStatus = _emailStatus;

    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    public void emailCheck(String email) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(email);
        _emailStatus.postValue(matcher.find());
    }
    // OLD MAIL IMPLEMENTATION



    private final MutableLiveData<ResultState<CaptchaDataModel>> _captchaState = new MutableLiveData<>();
    public LiveData<ResultState<CaptchaDataModel>> captchaState = _captchaState;

    public void getCaptcha() {
        AsyncTask.execute(() -> {
            _captchaState.postValue(ResultState.loading());
            _captchaState.postValue(captchaRepository.getCaptcha());
        });
    }
}
