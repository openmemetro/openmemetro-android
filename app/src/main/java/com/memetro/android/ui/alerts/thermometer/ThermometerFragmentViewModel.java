package com.memetro.android.ui.alerts.thermometer;

import static com.memetro.android.ui.dashboard.DashboardFilterViewModel.*;

import android.os.AsyncTask;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.memetro.android.api.ResultState;
import com.memetro.android.api.alerts.AlertsRepository;
import com.memetro.android.api.banner.BannerRepository;
import com.memetro.android.api.banner.models.BannerDataModel;
import com.memetro.android.local.dao.models.UIAlert;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;

@HiltViewModel
public class ThermometerFragmentViewModel extends ViewModel {

    private final AlertsRepository alertsRepository;
    private final BannerRepository bannerRepository;

    final MediatorLiveData<List<UIAlert>> alertsMediatorLiveData = new MediatorLiveData<>();

    @Inject
    public ThermometerFragmentViewModel(AlertsRepository alertsRepository, BannerRepository bannerRepository) {
        this.alertsRepository = alertsRepository;
        this.bannerRepository = bannerRepository;
        getAlerts();
        getBanner();
        alertsMediatorLiveData.addSource(alertsLiveData, alerts -> {
            if (alerts != null) {
                alertsMediatorLiveData.setValue(applyFilter(alerts));
            }
        });
    }

    private MutableLiveData<List<UIAlert>> _alertsLiveData = new MutableLiveData<>();
    private LiveData<List<UIAlert>> alertsLiveData = _alertsLiveData;

    public void getAlerts() {
        AsyncTask.execute(() -> {
            _alertsLiveData.postValue(alertsRepository.getAlerts());
        });
    }

    private AlertFilter alertFilter = new AlertFilter();

    public void filterAlerts(AlertFilter alertFilter) {
        this.alertFilter = alertFilter;
        if (_alertsLiveData.getValue() != null) {
            alertsMediatorLiveData.setValue(applyFilter(_alertsLiveData.getValue()));
        }
    }

    private List<UIAlert> applyFilter(List<UIAlert> alerts) {
        List<UIAlert> filtered = new ArrayList<>();
        for (int i = 0; i < alerts.size(); i++) {
            UIAlert alert = alerts.get(i);
            if (alertFilter.alertIsValid(alert)) {
                filtered.add(alert);
            }
        }
        return filtered;
    }

    public void getAlertsSync() {
        AsyncTask.execute(() -> {
            alertsRepository.getAlertsSync(false);
            getAlerts();
        });
    }

    private MutableLiveData<ResultState<List<BannerDataModel>>> _bannersLiveData = new MutableLiveData<>();
    public LiveData<ResultState<List<BannerDataModel>>> bannersLiveData = _bannersLiveData;

    public void getBanner() {
        AsyncTask.execute(() -> {
            _bannersLiveData.postValue(bannerRepository.getBanner());
        });
    }

}
