package com.memetro.android.ui;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewbinding.ViewBinding;

public abstract class BaseFragment<VBinding> extends Fragment {

    protected VBinding binding;

    protected abstract VBinding getBinding();

    protected abstract void observeViewModel();

    protected abstract void setUpViews(@Nullable Bundle savedInstanceState);

    protected abstract void onBackPressed();

    protected abstract void initViewModels();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = getBinding();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return ((ViewBinding) binding).getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUpViews(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        initViewModels();
        observeViewModel();
    }
}
