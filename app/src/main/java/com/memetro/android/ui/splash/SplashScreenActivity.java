/*
 * Copyright 2013 Nytyr [me at nytyr dot me]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.memetro.android.ui.splash;

//import com.crashlytics.android.Crashlytics;
import java.util.Timer;
import java.util.TimerTask;
import android.os.Bundle;
import android.content.Intent;

import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.memetro.android.databinding.ActivitySplashScreenBinding;
import com.memetro.android.api.ResultState;
import com.memetro.android.api.login.models.LoginResponseModel;
import com.memetro.android.ui.BaseActivity;
import com.memetro.android.ui.common.MemetroProgress;
import com.memetro.android.ui.dashboard.DashboardActivity;
import com.memetro.android.ui.login.LoginActivity;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class SplashScreenActivity extends BaseActivity<ActivitySplashScreenBinding> {

    private long splashDelay = 400; //0.4 sec

    private MemetroProgress pdialog;

    private SplashScreenViewModel viewModel;

    @Override
    protected void setUpViews(@Nullable Bundle savedInstanceState) {
        pdialog = new MemetroProgress(this);
        if(viewModel.isAutoLoginEnabled()) {
            viewModel.autoLogin();
        }else {
            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    goToLoginScreen();
                }
            };

            Timer timer = new Timer();
            timer.schedule(task, splashDelay);
        }
    }

    private void goToLoginScreen() {
        Intent mainIntent = new Intent().setClass(SplashScreenActivity.this, LoginActivity.class);
        mainIntent.addFlags(android.content.Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(mainIntent);
        finish();
    }

    private void handleAutoLogin(ResultState<LoginResponseModel> autoLoginState) {
        switch (autoLoginState.getStatus()) {
            case SUCCESS:
                if (pdialog.isShowing()) {
                    pdialog.dismiss();
                    Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
                    startActivity(intent);
                    finish();
                }
                break;
            case ERROR:
                if (pdialog.isShowing()) pdialog.dismiss();
                goToLoginScreen();
                break;
            case LOADING:
                pdialog.show();
                break;
        }
    }

    @Override
    protected ActivitySplashScreenBinding getBinding() {
        return ActivitySplashScreenBinding.inflate(getLayoutInflater());
    }

    @Override
    protected void initViewModels() {
        viewModel = new ViewModelProvider(this).get(SplashScreenViewModel.class);
    }

    @Override
    protected void observeViewModel() {
        viewModel.autoLoginState.observe(this, this::handleAutoLogin);
    }
}
