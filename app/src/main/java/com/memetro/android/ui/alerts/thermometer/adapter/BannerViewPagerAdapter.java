package com.memetro.android.ui.alerts.thermometer.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.memetro.android.R;
import com.memetro.android.api.banner.models.BannerDataModel;

import java.util.Collections;
import java.util.List;

public class BannerViewPagerAdapter extends RecyclerView.Adapter<BannerViewPagerAdapter.BannerViewHolder> {

    private List<BannerDataModel> banners = Collections.emptyList();
    private final Context ctx;

    public BannerViewPagerAdapter(Context ctx) {
        this.ctx = ctx;
    }

    @NonNull
    @Override
    public BannerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(ctx).inflate(R.layout.item_banner, parent, false);
        return new BannerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BannerViewHolder holder, int position) {
        holder.bind(banners.get(position));
    }

    @Override
    public int getItemCount() {
        return banners.size();
    }

    public void setItems(List<BannerDataModel> banners) {
        this.banners = banners;
        notifyDataSetChanged();
    }

    public static class BannerViewHolder extends RecyclerView.ViewHolder {
        TextView bannerTextView;
        ImageView bannerImage;
        CardView bannerContainer;

        public BannerViewHolder(@NonNull View itemView) {
            super(itemView);
            bannerTextView = itemView.findViewById(R.id.bannerTextView);
            bannerImage = itemView.findViewById(R.id.bannerImage);
            bannerContainer = itemView.findViewById(R.id.banner);
        }

        public void bind(BannerDataModel banner) {
            bannerTextView.setVisibility(View.VISIBLE);
            bannerTextView.setText(banner.getText());
            Integer hexTextColor = sanitizeColorHex(banner.getTextColor());
            if(hexTextColor != null) {
                bannerTextView.setTextColor(
                        hexTextColor
                );
            }
            if(banner.getBackgroundImage() != null && !banner.getBackgroundImage().isEmpty()){
                Glide.with(this.itemView.getContext())
                        .load(banner.getBackgroundImage())
                        .centerCrop()
                        .into(bannerImage);
            } else {
                Integer hexBackgroundColor = sanitizeColorHex(banner.getBackgroundColor());
                if(hexBackgroundColor != null) {
                    bannerTextView.setBackgroundColor(
                            hexBackgroundColor
                    );
                }
            }
            if(banner.getUrl() != null && !banner.getUrl().isEmpty()) {
                bannerContainer.setOnClickListener(view -> {
                    Intent browserIntent = new Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse(banner.getUrl()));
                    this.itemView.getContext().startActivity(browserIntent);
                });
            }
        }

        private Integer sanitizeColorHex(String hex) {
            String hexColor;
            if(hex.length() > 7) {
                hexColor = hex.substring(0, 7);
            } else {
                hexColor = hex;
            }
            if(!hexColor.isEmpty()) {
                try {
                    return Color.parseColor(hexColor);
                }catch (Exception e){
                    return null;
                }
            }
            return null;
        }
    }


}
