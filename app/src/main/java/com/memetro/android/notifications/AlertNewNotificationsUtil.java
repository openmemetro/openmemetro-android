package com.memetro.android.notifications;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.memetro.android.R;
import com.memetro.android.ui.splash.SplashScreenActivity;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

public class AlertNewNotificationsUtil {
    public static final String CHANNEL_ID = "AlertNewNotificationChanel";

    public final Context applicationContext;

    @Inject
    public AlertNewNotificationsUtil(Context applicationContext) {
        this.applicationContext = applicationContext;
    }

    public void showNotification() {
        NotificationManagerCompat.from(applicationContext)
                .notify(100, getNotification(
                        "Hay avisos nuevos!",
                        "Hay avisos nuevos!",
                        new HashMap<>()
                ));
    }

    private Notification getNotification(String title, String body, Map<String, String> params) {
        createNotificationChannel();
        Intent notificationIntent = new Intent(applicationContext, SplashScreenActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(applicationContext, 0, notificationIntent, 0);
        final NotificationCompat.Builder notification = new NotificationCompat.Builder(applicationContext, CHANNEL_ID)
                .setContentTitle(title)
                .setContentText(body)
                .setAutoCancel(true)
                .setSmallIcon(R.drawable.actionbar_logo)
                .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND)
                .setVibrate(null) // Passing null here silently fails
                .setContentIntent(pendingIntent);
        return notification.build();
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "New alert channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );

            NotificationManager manager = applicationContext.getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }

}
