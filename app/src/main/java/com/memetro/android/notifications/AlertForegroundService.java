package com.memetro.android.notifications;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.memetro.android.R;
import com.memetro.android.api.alerts.AlertsRepository;
import com.memetro.android.api.banner.BannerRepository;
import com.memetro.android.ui.dashboard.DashboardActivity;
import com.memetro.android.ui.splash.SplashScreenActivity;
import com.memetro.android.ui.splash.SplashScreenViewModel;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class AlertForegroundService extends Service {
    public static final String CHANNEL_ID = "ForegroundServiceChannel";
    private static final long DELAYED_TIME = 5;

    String ACTION_STOP_SERVICE= "STOP";
    ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);

    @Inject
    public AlertsRepository alertsRepository;

    @Inject
    public BannerRepository bannerRepository;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (ACTION_STOP_SERVICE.equals(intent.getAction())) {
            executor.shutdown();
            stopSelf();
            return START_NOT_STICKY;
        }

        Intent stopSelf = new Intent(this, AlertForegroundService.class);
        stopSelf.setAction(ACTION_STOP_SERVICE);

        PendingIntent pStopSelf = PendingIntent
                .getService(this, 0, stopSelf
                        ,PendingIntent.FLAG_CANCEL_CURRENT);
        createNotificationChannel();
        Intent notificationIntent = new Intent(this, DashboardActivity.class);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        final NotificationCompat.Builder notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("Memetro: Buscando alertas")
                .setContentText("Cerrar esta notificación impide que recibas avisos")
                .setSmallIcon(R.drawable.actionbar_logo)
                .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND)
                .addAction(R.drawable.actionbar_logo,"Close", pStopSelf)
                .setVibrate(null) // Passing null here silently fails
                .setContentIntent(pendingIntent);

        executor.scheduleAtFixedRate(this::scheduledAction, 0, DELAYED_TIME, TimeUnit.MINUTES);

        startForeground(1,notification.build());

        return START_NOT_STICKY;
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Foreground Service Channel",
                    NotificationManager.IMPORTANCE_LOW
            );
            serviceChannel.enableVibration(false);

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }

    private void scheduledAction () {
        alertsRepository.getAlertsSync(true);
        bannerRepository.getBanner();
    }
}
