package com.memetro.android.di;

import android.content.Context;

import com.memetro.android.local.AppDatabase;
import com.memetro.android.local.dao.*;
import com.memetro.android.local.datasource.alert.AlertsLocalDatasource;
import com.memetro.android.local.datasource.sync.SyncLocalDatasource;
import com.memetro.android.local.datasource.user.UserLocalDatasource;
import com.memetro.android.sharedprefs.SharedPrefs;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.qualifiers.ApplicationContext;
import dagger.hilt.components.SingletonComponent;

@Module
@InstallIn(SingletonComponent.class)
public class LocalModule {

    @Singleton
    @Provides
    public static AppDatabase provideAppDatabase(@ApplicationContext Context context) {
        return AppDatabase.getDatabase(context);
    }

    @Singleton
    @Provides
    public static CitiesTransportDao provideCitiesTransportDao(AppDatabase appDatabase) {
        return appDatabase.citiesTransportDao();
    }

    @Singleton
    @Provides
    public static CityDao provideCityDao(AppDatabase appDatabase) {
        return appDatabase.cityDao();
    }

    @Singleton
    @Provides
    public static CountryDao provideCountryDao(AppDatabase appDatabase) {
        return appDatabase.countryDao();
    }

    @Singleton
    @Provides
    public static LineDao provideLineDao(AppDatabase appDatabase) {
        return appDatabase.lineDao();
    }

    @Singleton
    @Provides
    public static LineStationDao provideLineStationDao(AppDatabase appDatabase) {
        return appDatabase.lineStationDao();
    }

    @Singleton
    @Provides
    public static StationDao provideStationDao(AppDatabase appDatabase) {
        return appDatabase.stationDao();
    }

    @Singleton
    @Provides
    public static TransportDao provideTransportDao(AppDatabase appDatabase) {
        return appDatabase.transportDao();
    }

    @Singleton
    @Provides
    public static AlertsDao provideAlertsDao(AppDatabase appDatabase) {
        return appDatabase.alertsDao();
    }

    @Singleton
    @Provides
    public static AlertTypeDao provideAlertsTypeDao(AppDatabase appDatabase) {
        return appDatabase.alertTypeDao();
    }

    @Singleton
    @Provides
    public static SyncLocalDatasource provideSyncLocalDatasource(
            CitiesTransportDao citiesTransportDao,
            CityDao cityDao,
            CountryDao countryDao,
            LineDao lineDao,
            LineStationDao lineStationDao,
            StationDao stationDao,
            TransportDao transportDao,
            AlertTypeDao alertTypeDao,
            SharedPrefs sharedPrefs
    ) {
        return new SyncLocalDatasource(
                citiesTransportDao,
                cityDao,
                countryDao,
                lineDao,
                lineStationDao,
                stationDao,
                transportDao,
                alertTypeDao,
                sharedPrefs
        );
    }

    @Singleton
    @Provides
    public static UserLocalDatasource provideUserLocalDatasource(SharedPrefs sharedPrefs) {
        return new UserLocalDatasource(sharedPrefs);
    }

    @Singleton
    @Provides
    public static AlertsLocalDatasource provideAlertsLocalDatasource(AlertsDao alertsDao) {
        return new AlertsLocalDatasource(alertsDao);
    }

}
