package com.memetro.android.di;

import com.memetro.android.BuildConfig;
import com.google.gson.Gson;
import com.memetro.android.api.login.RefreshTokenService;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.components.SingletonComponent;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
@InstallIn(SingletonComponent.class)
public class RefreshTokenModule {

    @Provides
    @Named("RefreshTokenOkHttp")
    public static OkHttpClient provideOkHttpClient() {
        HttpLoggingInterceptor httpLogger = new HttpLoggingInterceptor();
        if (BuildConfig.DEBUG) {
            httpLogger.level(HttpLoggingInterceptor.Level.BODY);
        } else {
            httpLogger.level(HttpLoggingInterceptor.Level.NONE);
        }
        return new OkHttpClient.Builder()
                .addInterceptor(httpLogger)
                .build();
    }

    @Singleton
    @Named("RefreshTokenRetrofit")
    @Provides
    public static Retrofit provideRetrofit(@Named("RefreshTokenOkHttp") OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.OAUTHSERVER)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .build();
    }

    @Singleton
    @Provides
    public static RefreshTokenService provideRefreshTokenService(@Named("RefreshTokenRetrofit") Retrofit retrofit) {
        return retrofit.create(RefreshTokenService.class);
    }
}
