package com.memetro.android.di;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.security.crypto.EncryptedSharedPreferences;
import androidx.security.crypto.MasterKey;

import com.memetro.android.sharedprefs.SharedPrefs;
import com.memetro.android.sharedprefs.SharedPrefsImpl;

import java.io.IOException;
import java.security.GeneralSecurityException;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.qualifiers.ApplicationContext;
import dagger.hilt.components.SingletonComponent;

@Module
@InstallIn(SingletonComponent.class)
public class SharedPrefsModule {

    @Provides
    @Singleton
    public static SharedPreferences provideSharedPreferences(@ApplicationContext Context context)  {
        try {
            return EncryptedSharedPreferences.create(
                    context,
                    SharedPrefs.SHARED_PREFS_NAME,
                    new MasterKey.Builder(context).setKeyScheme(MasterKey.KeyScheme.AES256_GCM).build(),
                    EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                    EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM);
        } catch (Exception e) {
            return null;
        }
    }

    @Provides
    @Singleton
    public static SharedPrefs provideSharedPrefs(SharedPreferences sharedPreferences) {
        return new SharedPrefsImpl(sharedPreferences);
    }
}
