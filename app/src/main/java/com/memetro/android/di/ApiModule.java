package com.memetro.android.di;

import android.content.Context;

import com.memetro.android.api.alerts.AlertsRemoteDatasource;
import com.memetro.android.api.alerts.AlertsRepository;
import com.memetro.android.api.alerts.AlertsService;
import com.memetro.android.api.banner.BannerRemoteDataSource;
import com.memetro.android.api.banner.BannerRepository;
import com.memetro.android.api.banner.BannerService;
import com.memetro.android.api.captcha.CaptchaRemoteDataSource;
import com.memetro.android.api.captcha.CaptchaRepository;
import com.memetro.android.api.captcha.CaptchaService;
import com.memetro.android.api.login.LoginService;
import com.memetro.android.api.login.RefreshTokenService;
import com.memetro.android.api.signup.SignupRemoteDataSource;
import com.memetro.android.api.signup.SignupRepository;
import com.memetro.android.api.signup.SignupService;
import com.memetro.android.api.sync.*;
import com.memetro.android.api.login.LoginRemoteDatasource;
import com.memetro.android.api.login.LoginRepository;
import com.memetro.android.api.user.UserRemoteDatasource;
import com.memetro.android.api.user.UserRepository;
import com.memetro.android.local.datasource.alert.AlertsLocalDatasource;
import com.memetro.android.local.datasource.sync.SyncLocalDatasource;
import com.memetro.android.local.datasource.user.UserLocalDatasource;
import com.memetro.android.notifications.AlertNewNotificationsUtil;
import com.memetro.android.sharedprefs.SharedPrefs;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.qualifiers.ApplicationContext;
import dagger.hilt.components.SingletonComponent;
import retrofit2.Retrofit;

@Module
@InstallIn(SingletonComponent.class)
public class ApiModule {

    @Singleton
    @Provides
    public LoginService provideOauthService(@Named("GeneralRetrofit") Retrofit retrofit) {
        return retrofit.create(LoginService.class);
    }

    @Singleton
    @Provides
    public LoginRemoteDatasource provideLoginDatasource(LoginService service, RefreshTokenService refreshTokenService) {
        return new LoginRemoteDatasource(service, refreshTokenService);
    }

    @Singleton
    @Provides
    public LoginRepository provideLoginRepository(LoginRemoteDatasource datasource, SharedPrefs sharedPrefs) {
        return new LoginRepository(datasource, sharedPrefs);
    }


    @Singleton
    @Provides
    public SyncService provideSyncService(@Named("GeneralRetrofit") Retrofit retrofit) {
        return retrofit.create(SyncService.class);
    }

    @Singleton
    @Provides
    public SyncRemoteDatasource provideSyncDatasource(SyncService service) {
        return new SyncRemoteDatasource(service);
    }

    @Singleton
    @Provides
    public AlertsRemoteDatasource provideAlertsRemoteDatasource(AlertsService service) {
        return new AlertsRemoteDatasource(service);
    }

    @Singleton
    @Provides
    public SyncRepository provideSyncRepository(SyncRemoteDatasource datasource, SyncLocalDatasource syncLocalDatasource) {
        return new SyncRepository(datasource, syncLocalDatasource);
    }

    @Singleton
    @Provides
    public UserRepository provideUserRepository(UserRemoteDatasource userRemoteDatasource, UserLocalDatasource userLocalDatasource) {
        return new UserRepository(userRemoteDatasource, userLocalDatasource);
    }

    @Singleton
    @Provides
    public AlertNewNotificationsUtil provideAlertNewNotificationsUtil(@ApplicationContext Context context) {
        return new AlertNewNotificationsUtil(context);
    }

    @Singleton
    @Provides
    public AlertsRepository provideAlertRepository(AlertsRemoteDatasource alertsRemoteDatasource, AlertsLocalDatasource alertsLocalDatasource, AlertNewNotificationsUtil alertNewNotificationsUtil) {
        return new AlertsRepository(alertsRemoteDatasource, alertsLocalDatasource, alertNewNotificationsUtil);
    }

    @Singleton
    @Provides
    public AlertsService provideAlertsService(@Named("GeneralRetrofit") Retrofit retrofit) {
        return retrofit.create(AlertsService.class);
    }

    @Singleton
    @Provides
    public SignupService provideSignupService(@Named("RefreshTokenRetrofit") Retrofit retrofit) {
        return retrofit.create(SignupService.class);
    }

    @Singleton
    @Provides
    public SignupRepository provideSignupRepository(SignupRemoteDataSource signupDatasource) {
        return new SignupRepository(signupDatasource);
    }

    @Singleton
    @Provides
    public SignupRemoteDataSource signupRemoteDatasource(SignupService signupService) {
        return new SignupRemoteDataSource(signupService);
    }

    @Singleton
    @Provides
    public StaticDataService provideStaticDataService(@Named("RefreshTokenRetrofit") Retrofit retrofit) {
        return retrofit.create(StaticDataService.class);
    }

    @Singleton
    @Provides
    public StaticDataRepository provideStaticDataRepository(StaticDataRemoteDatasource staticDataDatasource,  SyncLocalDatasource syncLocalDatasource) {
        return new StaticDataRepository(staticDataDatasource, syncLocalDatasource);
    }

    @Singleton
    @Provides
    public StaticDataRemoteDatasource staticDataRemoteDatasource(StaticDataService staticDataService) {
        return new StaticDataRemoteDatasource(staticDataService);
    }

    @Singleton
    @Provides
    public BannerService provideBannerService(@Named("GeneralRetrofit") Retrofit retrofit) {
        return retrofit.create(BannerService.class);
    }

    @Singleton
    @Provides
    public BannerRemoteDataSource provideRemoteBannerDatasource(BannerService service) {
        return new BannerRemoteDataSource(service);
    }

    @Singleton
    @Provides
    public BannerRepository provideBannerRepository(BannerRemoteDataSource remoteDatasource) {
        return new BannerRepository(remoteDatasource);
    }

    ////////////////////////////////////////////

    @Singleton
    @Provides
    public CaptchaService provideCaptchaService(@Named("GeneralRetrofit") Retrofit retrofit) {
        return retrofit.create(CaptchaService.class);
    }

    @Singleton
    @Provides
    public CaptchaRemoteDataSource provideRemoteCaptchaDatasource(CaptchaService service) {
        return new CaptchaRemoteDataSource(service);
    }

    @Singleton
    @Provides
    public CaptchaRepository provideCaptchaRepository(CaptchaRemoteDataSource remoteDatasource) {
        return new CaptchaRepository(remoteDatasource);
    }

}
