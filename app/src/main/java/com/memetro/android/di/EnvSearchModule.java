package com.memetro.android.di;

import com.memetro.android.BuildConfig;
import com.google.gson.Gson;
import com.memetro.android.api.enviroments.EnvSearchRemoteDatasource;
import com.memetro.android.api.enviroments.EnvSearchService;
import com.memetro.android.api.login.RefreshTokenService;
import com.memetro.android.api.sync.SyncRemoteDatasource;
import com.memetro.android.api.sync.SyncService;
import com.memetro.android.api.utils.HostSelectionInterceptor;
import com.memetro.android.api.utils.TokenAuthenticator;
import com.memetro.android.sharedprefs.SharedPrefs;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.components.SingletonComponent;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
@InstallIn(SingletonComponent.class)
public class EnvSearchModule {

    @Provides
    @Named("EnvSearchOkHttp")
    public static OkHttpClient provideOkHttpClient() {
        HttpLoggingInterceptor httpLogger = new HttpLoggingInterceptor();
        if (BuildConfig.DEBUG) {
            httpLogger.level(HttpLoggingInterceptor.Level.BODY);
        } else {
            httpLogger.level(HttpLoggingInterceptor.Level.NONE);
        }
        return new OkHttpClient.Builder()
                .addInterceptor(httpLogger)
                .build();
    }

    @Singleton
    @Named("EnvSearchRetrofit")
    @Provides
    public static Retrofit provideRetrofit(@Named("EnvSearchOkHttp") OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.ENVLIST)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .build();
    }

    @Singleton
    @Provides
    public static HostSelectionInterceptor provideHostSelectionInterceptor(EnvSearchRemoteDatasource envSearchRemoteDatasource) {
        return new HostSelectionInterceptor(envSearchRemoteDatasource);
    }

    @Singleton
    @Provides
    public static EnvSearchService provideEnvSearchService(@Named("EnvSearchRetrofit") Retrofit retrofit) {
        return retrofit.create(EnvSearchService.class);
    }

    @Singleton
    @Provides
    public EnvSearchRemoteDatasource provideEnvSearchDatasource(EnvSearchService service) {
        return new EnvSearchRemoteDatasource(service);
    }
}
