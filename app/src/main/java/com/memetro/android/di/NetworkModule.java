package com.memetro.android.di;

import com.memetro.android.BuildConfig;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.memetro.android.api.login.RefreshTokenService;
import com.memetro.android.api.utils.AuthorizationInterceptor;
import com.memetro.android.api.utils.TokenAuthenticator;
import com.memetro.android.sharedprefs.SharedPrefs;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.qualifiers.ApplicationContext;
import dagger.hilt.components.SingletonComponent;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
@InstallIn(SingletonComponent.class)
public class NetworkModule {

    @Provides
    @Named("GeneralOkHttp")
    public static OkHttpClient provideOkHttpClient(
            AuthorizationInterceptor authorizationInterceptor,
            TokenAuthenticator tokenAuthenticator
    ) {
        HttpLoggingInterceptor httpLogger = new HttpLoggingInterceptor();
        if (BuildConfig.DEBUG) {
            httpLogger.level(HttpLoggingInterceptor.Level.BODY);
        } else {
            httpLogger.level(HttpLoggingInterceptor.Level.NONE);
        }
        return new OkHttpClient.Builder()
                .addInterceptor(httpLogger)
                .addInterceptor(authorizationInterceptor)
                .authenticator(tokenAuthenticator)
                //.addInterceptor(hostSelectionInterceptor)
                .build();
    }

    @Singleton
    @Named("GeneralRetrofit")
    @Provides
    public static Retrofit provideRetrofit(@Named("GeneralOkHttp") OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BuildConfig.OAUTHSERVER)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'").create()))
                .build();
    }

    @Singleton
    @Provides
    public static AuthorizationInterceptor provideAuthorizationInterceptor(SharedPrefs sharedPrefs) {
        return new AuthorizationInterceptor(sharedPrefs);
    }

    @Singleton
    @Provides
    public static TokenAuthenticator provideTokenAuthenticator(SharedPrefs sharedPrefs, RefreshTokenService refreshTokenService) {
        return new TokenAuthenticator(sharedPrefs, refreshTokenService);
    }

}
