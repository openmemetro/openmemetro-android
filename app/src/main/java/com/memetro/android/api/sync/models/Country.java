package com.memetro.android.api.sync.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "Country", indices = {@Index(value = {"id"}, unique = true)})
public class Country {

    @ColumnInfo(name = "name")
    @SerializedName("name")
    String name;

    @PrimaryKey
    @ColumnInfo(name = "id")
    @SerializedName("id")
    Integer id;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    @Override
    public String toString() {
        return name;
    }
}
