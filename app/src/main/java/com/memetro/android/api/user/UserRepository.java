package com.memetro.android.api.user;

import com.memetro.android.api.sync.models.User;
import com.memetro.android.local.datasource.user.UserLocalDatasource;

public class UserRepository {

    private final UserRemoteDatasource userRemoteDatasource;
    private final UserLocalDatasource userLocalDatasource;

    public UserRepository(UserRemoteDatasource userRemoteDatasource, UserLocalDatasource userLocalDatasource) {
        this.userRemoteDatasource = userRemoteDatasource;
        this.userLocalDatasource = userLocalDatasource;
    }

    public User getUser() {
        return userLocalDatasource.getUser();
    }

    public void logout() {
        userLocalDatasource.logout();
    }
}
