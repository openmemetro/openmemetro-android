package com.memetro.android.api.sync.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(
        tableName = "LineStation",
        indices = {
                @Index(value = {"line_id", "station_id"}, unique = true),
                @Index(value = {"line_id"}),
                @Index(value = {"station_id"})
        },
        foreignKeys = {
                @ForeignKey(entity = Station.class, parentColumns = "id", childColumns = "station_id"),
                @ForeignKey(entity = Line.class, parentColumns = "id", childColumns = "line_id")
        }
)
public class LineStation {

    @PrimaryKey(autoGenerate = true)
    Integer id;

    @ColumnInfo(name = "line_id")
    @SerializedName("line_id")
    Integer lineId;

    @ColumnInfo(name = "station_id")
    @SerializedName("station_id")
    Integer stationId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setLineId(Integer lineId) {
        this.lineId = lineId;
    }

    public Integer getLineId() {
        return lineId;
    }

    public void setStationId(Integer stationId) {
        this.stationId = stationId;
    }

    public Integer getStationId() {
        return stationId;
    }
}
