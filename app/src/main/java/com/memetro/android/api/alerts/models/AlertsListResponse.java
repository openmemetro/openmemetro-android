package com.memetro.android.api.alerts.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AlertsListResponse {

    @SerializedName("sucess")
    boolean sucess;

    @SerializedName("message")
    String message;

    @SerializedName("data")
    List<Alert> alerts;

    public void setSucess(boolean sucess) {
        this.sucess = sucess;
    }
    public boolean getSucess() {
        return sucess;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    public String getMessage() {
        return message;
    }

    public void setAlerts(List<Alert> alerts) {
        this.alerts = alerts;
    }
    public List<Alert> getAlerts() {
        return alerts;
    }
}
