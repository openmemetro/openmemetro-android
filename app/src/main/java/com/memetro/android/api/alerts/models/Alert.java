package com.memetro.android.api.alerts.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.google.gson.annotations.SerializedName;
import com.memetro.android.api.alerts.models.converters.DateToTimeStampConverter;
import com.memetro.android.api.sync.models.AlertType;
import com.memetro.android.api.sync.models.City;
import com.memetro.android.api.sync.models.Line;
import com.memetro.android.api.sync.models.Station;
import com.memetro.android.api.sync.models.Transport;

import java.util.Date;

@Entity(tableName = "alert",
        indices = {
                @Index(value = {"line_id"}),
                @Index(value = {"city_id"}),
                @Index(value = {"station_id"}),
                @Index(value = {"transport_id"})
        },
        foreignKeys = {
            @ForeignKey(entity = Line.class, parentColumns = "id", childColumns = "line_id"),
            @ForeignKey(entity = City.class, parentColumns = "id", childColumns = "city_id"),
            @ForeignKey(entity = Station.class, parentColumns = "id", childColumns = "station_id"),
            @ForeignKey(entity = Transport.class, parentColumns = "id", childColumns = "transport_id"),
            @ForeignKey(entity = AlertType.class, parentColumns = "id", childColumns = "alert_type_id")
        }
)
public class Alert {

    @PrimaryKey
    @SerializedName("id")
    Integer id;

    @ColumnInfo(name = "description")
    @SerializedName("description")
    String description;

    @ColumnInfo(name = "date")
    @SerializedName("date")
    @TypeConverters({DateToTimeStampConverter.class})
    Date date;

    @ColumnInfo(name = "img_url")
    @SerializedName("img_url")
    String imgUrl;

    @ColumnInfo(name = "line_id")
    @SerializedName("line_id")
    Integer lineId;

    @ColumnInfo(name = "city_id")
    @SerializedName("city_id")
    Integer cityId;

    @ColumnInfo(name = "station_id")
    @SerializedName("station_id")
    Integer stationId;

    @ColumnInfo(name = "transport_id")
    @SerializedName("transport_id")
    Integer transportId;

    @ColumnInfo(name = "alert_type_id")
    @SerializedName("alert_type")
    Integer alertTypeId;

    @ColumnInfo(name = "active")
    @SerializedName("active")
    Boolean active;

    @Ignore
    Boolean expanded = false;

    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getId() {
        return id;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescription() {
        return description;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    public Date getDate() {
        return date;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
    public String getImgUrl() {
        return imgUrl;
    }

    public void setLineId(Integer lineId) {
        this.lineId = lineId;
    }
    public Integer getLineId() {
        return lineId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }
    public Integer getCityId() {
        return cityId;
    }

    public void setStationId(Integer stationId) {
        this.stationId = stationId;
    }
    public Integer getStationId() {
        return stationId;
    }

    public void setTransportId(Integer transportId) {
        this.transportId = transportId;
    }
    public Integer getTransportId() {
        return transportId;
    }

    public boolean isExpanded() {
        return expanded;
    }

    public Integer getAlertTypeId() {
        return alertTypeId;
    }

    public void setAlertTypeId(Integer alertTypeId) {
        this.alertTypeId = alertTypeId;
    }

    public void setExpanded(Boolean expanded) {
        this.expanded = expanded;
    }

    public Boolean getExpanded() {
        return expanded;
    }

    public Boolean getActive() { return active; }

    public void setActive(Boolean active) { this.active = active; }

}
