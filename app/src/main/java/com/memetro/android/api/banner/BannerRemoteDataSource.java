package com.memetro.android.api.banner;

import com.memetro.android.api.BaseRemoteDatasource;
import com.memetro.android.api.ResultState;
import com.memetro.android.api.banner.models.BannerDataModel;
import com.memetro.android.api.banner.models.BannerResponseModel;

import java.util.List;

public class BannerRemoteDataSource extends BaseRemoteDatasource {
    private final BannerService bannerService;

    public BannerRemoteDataSource(BannerService bannerService) {
        this.bannerService = bannerService;
    }

    public ResultState<List<BannerDataModel>> getBanner( ) {
        return getResult(bannerService.getBanner());
    }
}
