package com.memetro.android.api.alerts;

import com.memetro.android.api.BaseRemoteDatasource;
import com.memetro.android.api.ResultState;
import com.memetro.android.api.alerts.models.Alert;
import com.memetro.android.api.alerts.models.AlertCreationRequest;
import com.memetro.android.api.alerts.models.AlertCreationResponse;
import com.memetro.android.api.alerts.models.AlertsListResponse;

import java.util.List;

public class AlertsRemoteDatasource extends BaseRemoteDatasource {

    private final AlertsService alertsService;

    public AlertsRemoteDatasource(AlertsService alertsService) {
        this.alertsService = alertsService;
    }

    public ResultState<List<Alert>> getAlerts() {
        return getResult(alertsService.getAlerts());
    }

    public ResultState<AlertCreationResponse> createAlert(
            AlertCreationRequest request
    ) {
        return getResult(alertsService.createAlert(request));
    }
}
