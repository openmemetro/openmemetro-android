package com.memetro.android.api.signup;

import com.memetro.android.api.ResultState;
import com.memetro.android.api.signup.models.SignupRequestModel;
import com.memetro.android.api.signup.models.SignupResponseModel;

public class SignupRepository {

    private final SignupRemoteDataSource signupDatasource;

    public SignupRepository(SignupRemoteDataSource signupDatasource) {
        this.signupDatasource = signupDatasource;
    }

    public ResultState<SignupResponseModel> register(SignupRequestModel signupRequestModel) {
        return signupDatasource.register(signupRequestModel);
    }
}
