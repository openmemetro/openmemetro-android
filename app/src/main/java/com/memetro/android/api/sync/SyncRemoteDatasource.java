package com.memetro.android.api.sync;

import com.memetro.android.api.BaseRemoteDatasource;
import com.memetro.android.api.ResultState;
import com.memetro.android.api.sync.models.SyncResponseModel;

public class SyncRemoteDatasource extends BaseRemoteDatasource {

    private final SyncService syncService;

    public SyncRemoteDatasource(SyncService syncService) {
        this.syncService = syncService;
    }

    public ResultState<SyncResponseModel> sync() {
        return getResult(syncService.sync());
    }

}
