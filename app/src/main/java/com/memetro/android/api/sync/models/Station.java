package com.memetro.android.api.sync.models;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "Station", indices = {@Index(value = {"id"}, unique = true)})
public class Station {
    @SerializedName("name")
    @ColumnInfo(name = "name")
    String name;

    @PrimaryKey
    @ColumnInfo(name = "id")
    @SerializedName("id")
    Integer id;

    @SerializedName("longitude")
    @ColumnInfo(name = "longitude")
    String longitude;

    @SerializedName("latitude")
    @ColumnInfo(name = "latitude")
    String latitude;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLatitude() {
        return latitude;
    }

    @NonNull
    @Override
    public String toString() {
        return name;
    }
}
