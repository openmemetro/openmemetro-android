package com.memetro.android.api.sync.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LinesStations {
    @SerializedName("data")
    List<LineStation> data;

    public void setData(List<LineStation> data) {
        this.data = data;
    }

    public List<LineStation> getData() {
        return data;
    }
}

