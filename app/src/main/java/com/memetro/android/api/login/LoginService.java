package com.memetro.android.api.login;

import com.memetro.android.api.login.models.LoginRequestModel;
import com.memetro.android.api.login.models.LoginResponseModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface LoginService {

    @POST("/api/token/")
    Call<LoginResponseModel> login(
            @Body LoginRequestModel loginRequestBody
    );

}
