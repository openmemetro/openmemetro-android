package com.memetro.android.api.sync;

import com.memetro.android.api.ResultState;
import com.memetro.android.api.sync.models.StaticDataResponseModel;
import com.memetro.android.local.datasource.sync.SyncLocalDatasource;

public class StaticDataRepository {

    private final StaticDataRemoteDatasource staticDataRemoteDatasource;
    private final SyncLocalDatasource syncLocalDatasource;

    public StaticDataRepository(StaticDataRemoteDatasource staticDataRemoteDatasource, SyncLocalDatasource syncLocalDatasource) {
        this.staticDataRemoteDatasource = staticDataRemoteDatasource;
        this.syncLocalDatasource = syncLocalDatasource;
    }

    public ResultState<StaticDataResponseModel> sync() {
        ResultState<StaticDataResponseModel> result = staticDataRemoteDatasource.sync();
        if (result.getStatus() == ResultState.Status.SUCCESS
                && result.getData() != null
        ) {
            StaticDataResponseModel staticDataResponseModel = result.getData();
            syncLocalDatasource.sync(staticDataResponseModel);
        }
        return result;
    }
}
