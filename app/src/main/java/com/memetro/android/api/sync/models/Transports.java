package com.memetro.android.api.sync.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Transports {
    @SerializedName("data")
    List<Transport> data;

    public void setData(List<Transport> data) {
        this.data = data;
    }

    public List<Transport> getData() {
        return data;
    }
}

