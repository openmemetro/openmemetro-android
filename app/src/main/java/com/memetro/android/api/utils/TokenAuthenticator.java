package com.memetro.android.api.utils;

import androidx.annotation.Nullable;

import com.memetro.android.api.login.RefreshTokenService;
import com.memetro.android.api.login.models.LoginResponseModel;
import com.memetro.android.api.login.models.RefreshTokenModel;
import com.memetro.android.sharedprefs.SharedPrefs;

import org.apache.http.HttpStatus;

import java.io.IOException;

import okhttp3.Authenticator;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;

public class TokenAuthenticator implements Authenticator {

    private final SharedPrefs sharedPrefs;
    private final RefreshTokenService refreshTokenService;

    public TokenAuthenticator(
            SharedPrefs sharedPrefs,
            RefreshTokenService refreshTokenService
    ) {
        this.sharedPrefs = sharedPrefs;
        this.refreshTokenService = refreshTokenService;
    }

    @Nullable
    @Override
    public Request authenticate(@Nullable Route route, Response response) throws IOException {
        if(response.code() == HttpStatus.SC_UNAUTHORIZED) {
            RefreshTokenModel refreshTokenRequest = new RefreshTokenModel();
            refreshTokenRequest.setRefreshToken(sharedPrefs.getRefreshToken());
            retrofit2.Response refreshTokenResponse
                    = refreshTokenService.refreshToken(refreshTokenRequest).execute();
            if(refreshTokenResponse.isSuccessful()) {
                LoginResponseModel responseModel = (LoginResponseModel) refreshTokenResponse.body();
                if(responseModel!=null) {
                    sharedPrefs.saveToken(responseModel.getAccessToken());
                    sharedPrefs.saveRefreshToken(responseModel.getRefreshToken());
                    return response.request().newBuilder()
                            .header("Authorization", "Bearer " + responseModel.getAccessToken())
                            .build();
                }
            }
            return null;
        }
        return null;
    }

}
