package com.memetro.android.api.sync.models;

import com.google.gson.annotations.SerializedName;


public class SyncResponseModel {
    @SerializedName("data")
    Data data;

    public void setData(Data data) {
        this.data = data;
    }

    public Data getData() {
        return data;
    }
}

