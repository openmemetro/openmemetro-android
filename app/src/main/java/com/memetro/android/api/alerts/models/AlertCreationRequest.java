package com.memetro.android.api.alerts.models;

import com.google.gson.annotations.SerializedName;

public class AlertCreationRequest {

    @SerializedName("city_id")
    Integer cityId;

    @SerializedName("transport_id")
    Integer transportId;

    @SerializedName("comment")
    String comment;

    @SerializedName("line_id")
    Integer lineId;

    @SerializedName("station_id")
    Integer stationId;

    @SerializedName("alert_type")
    Integer alertTypeId;

    public AlertCreationRequest(Integer cityId, Integer transportId, String comment, Integer lineId, Integer stationId,  Integer alertTypeId) {
        this.cityId = cityId;
        this.transportId = transportId;
        this.comment = comment;
        this.lineId = lineId;
        this.stationId = stationId;
        this.alertTypeId = alertTypeId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }
    public Integer getCityId() {
        return cityId;
    }

    public void setTransportId(Integer transportId) {
        this.transportId = transportId;
    }
    public Integer getTransportId() {
        return transportId;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
    public String getComment() {
        return comment;
    }

    public void setLineId(Integer lineId) {
        this.lineId = lineId;
    }
    public Integer getLineId() {
        return lineId;
    }

    public void setStationId(Integer stationId) {
        this.stationId = stationId;
    }
    public Integer getStationId() {
        return stationId;
    }

    public Integer getAlertTypeId() {
        return alertTypeId;
    }

    public void setAlertTypeId(Integer alertTypeId) {
        this.alertTypeId = alertTypeId;
    }
}
