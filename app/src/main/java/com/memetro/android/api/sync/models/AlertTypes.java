package com.memetro.android.api.sync.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AlertTypes {
    @SerializedName("data")
    List<AlertType> data;

    public void setData(List<AlertType> data) {
        this.data = data;
    }

    public List<AlertType> getData() {
        return data;
    }
}