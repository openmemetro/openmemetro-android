package com.memetro.android.api.alerts.models;

import com.google.gson.annotations.SerializedName;

public class AddAlertRequest {

    @SerializedName("city_id")
    int cityId;

    @SerializedName("transport_id")
    int transportId;

    @SerializedName("comment")
    String comment;

    @SerializedName("line_id")
    int lineId;

    @SerializedName("station_id")
    int stationId;


    public void setCityId(int cityId) {
        this.cityId = cityId;
    }
    public int getCityId() {
        return cityId;
    }

    public void setTransportId(int transportId) {
        this.transportId = transportId;
    }
    public int getTransportId() {
        return transportId;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
    public String getComment() {
        return comment;
    }

    public void setLineId(int lineId) {
        this.lineId = lineId;
    }
    public int getLineId() {
        return lineId;
    }

    public void setStationId(int stationId) {
        this.stationId = stationId;
    }
    public int getStationId() {
        return stationId;
    }

}