package com.memetro.android.api.enviroments.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EnvListModel {
    @SerializedName("env")
    List<String> env;

    public List<String> getEnv() {
        return env;
    }

}
