package com.memetro.android.api.enviroments;

import com.memetro.android.BuildConfig;
import com.memetro.android.api.BaseRemoteDatasource;
import com.memetro.android.api.ResultState;
import com.memetro.android.api.alerts.models.Alert;
import com.memetro.android.api.enviroments.model.EnvListModel;
import com.memetro.android.api.sync.models.StaticDataResponseModel;

import java.util.List;

public class EnvSearchRemoteDatasource extends BaseRemoteDatasource {

    private final EnvSearchService envSearch;

    public EnvSearchRemoteDatasource(EnvSearchService envSearch) {
        this.envSearch = envSearch;
    }

    public ResultState<EnvListModel> getEnv() {
        return getResult(envSearch.getEnv());
    }

    public ResultState<StaticDataResponseModel> check() {
        return getResult(envSearch.check());
    }
}
