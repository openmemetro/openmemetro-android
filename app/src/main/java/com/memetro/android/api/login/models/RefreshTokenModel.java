package com.memetro.android.api.login.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RefreshTokenModel implements Serializable {

    @SerializedName("refresh")
    String refreshToken;

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }
    public String getRefreshToken() {
        return refreshToken;
    }
}
