package com.memetro.android.api.sync;

import androidx.lifecycle.LiveData;

import com.memetro.android.api.ResultState;
import com.memetro.android.api.sync.models.AlertType;
import com.memetro.android.api.sync.models.SyncResponseModel;
import com.memetro.android.api.sync.models.Transport;
import com.memetro.android.local.datasource.sync.SyncLocalDatasource;

import java.util.List;

public class SyncRepository {

    private final SyncRemoteDatasource syncDatasource;
    private final SyncLocalDatasource syncLocalDatasource;

    public SyncRepository(SyncRemoteDatasource syncDatasource, SyncLocalDatasource syncLocalDatasource) {
        this.syncDatasource = syncDatasource;
        this.syncLocalDatasource = syncLocalDatasource;
    }

    public ResultState<SyncResponseModel> sync() {
        ResultState<SyncResponseModel> syncResult = syncDatasource.sync();
        if (syncResult.getStatus() == ResultState.Status.SUCCESS
                && syncResult.getData() != null
        ) {
            SyncResponseModel syncResponseModel = syncResult.getData();
            syncLocalDatasource.sync(syncResponseModel);
        }
        return syncResult;
    }

    public LiveData<List<Transport>> getTransports() {
        return syncLocalDatasource.getTransports();
    }

    public LiveData<List<AlertType>> getAlertTypes() {
        return syncLocalDatasource.getAlertTypes();
    }
}
