package com.memetro.android.api.sync.models;

import com.google.gson.annotations.SerializedName;

public class Data {
    @SerializedName("user")
    User user;

    @SerializedName("country")
    Countries countries;

    @SerializedName("city")
    Cities cities;

    @SerializedName("line")
    Lines lines;

    @SerializedName("transport")
    Transports transport;

    @SerializedName("station")
    Stations stations;

    @SerializedName("citiestransport")
    CitiesTransports citiestransport;

    @SerializedName("linesstations")
    LinesStations linesstations;

    @SerializedName("alert_types")
    AlertTypes alertTypes;

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setCountry(Countries countries) {
        this.countries = countries;
    }

    public Countries getCountry() {
        return countries;
    }

    public void setCity(Cities cities) {
        this.cities = cities;
    }

    public Cities getCity() {
        return cities;
    }

    public void setLine(Lines lines) {
        this.lines = lines;
    }

    public Lines getLine() {
        return lines;
    }

    public void setTransport(Transports transport) {
        this.transport = transport;
    }

    public Transports getTransport() {
        return transport;
    }

    public void setStation(Stations station) {
        this.stations = station;
    }

    public Stations getStation() {
        return stations;
    }

    public void setCitiestransport(CitiesTransports citiestransport) {
        this.citiestransport = citiestransport;
    }

    public CitiesTransports getCitiestransport() {
        return citiestransport;
    }

    public void setLinesstations(LinesStations linesstations) {
        this.linesstations = linesstations;
    }

    public LinesStations getLinesstations() {
        return linesstations;
    }

    public AlertTypes getAlertTypes() {
        return alertTypes;
    }

    public void setAlertTypes(AlertTypes alertTypes) {
        this.alertTypes = alertTypes;
    }
}
