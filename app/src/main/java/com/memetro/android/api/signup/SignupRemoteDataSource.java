package com.memetro.android.api.signup;

import com.memetro.android.api.BaseRemoteDatasource;
import com.memetro.android.api.ResultState;
import com.memetro.android.api.signup.models.SignupRequestModel;
import com.memetro.android.api.signup.models.SignupResponseModel;

public class SignupRemoteDataSource extends BaseRemoteDatasource  {

    private final SignupService signupService;

    public SignupRemoteDataSource(SignupService signupService) {
        this.signupService = signupService;
    }

    public ResultState<SignupResponseModel> register(SignupRequestModel signupRequestModel) {
        return getResult(signupService.register(signupRequestModel));
    }
}
