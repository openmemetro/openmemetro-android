package com.memetro.android.api.alerts.models.converters;

import androidx.room.TypeConverter;

import com.memetro.android.utils.DateTimeUtils;

import java.util.Date;

public class DateToTimeStampConverter {
    @TypeConverter
    public Long dateStringToTime(Date value) {
        try {
            return value.getTime();
        } catch (Exception ignored) { }
        return null;
    }

    @TypeConverter
    public Date objectToJson(Long timestamp) {
        try {
            return new Date(timestamp);
        } catch (Exception ignored) { }
        return null;
    }
}