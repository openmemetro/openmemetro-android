package com.memetro.android.api.banner;

import com.memetro.android.api.banner.models.BannerDataModel;
import com.memetro.android.api.banner.models.BannerResponseModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface BannerService {
    @GET("/banner/")
    Call<List<BannerDataModel>> getBanner();
}
