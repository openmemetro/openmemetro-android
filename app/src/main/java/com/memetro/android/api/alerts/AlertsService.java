package com.memetro.android.api.alerts;

import com.memetro.android.api.alerts.models.Alert;
import com.memetro.android.api.alerts.models.AlertCreationRequest;
import com.memetro.android.api.alerts.models.AlertCreationResponse;
import com.memetro.android.api.alerts.models.AlertsListResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface AlertsService {

    @GET("/alerts/")
    Call<List<Alert>> getAlerts();

    @POST("/alert/")
    Call<AlertCreationResponse> createAlert(
            @Body AlertCreationRequest body
    );
}
