package com.memetro.android.api.banner;

import com.memetro.android.api.banner.models.BannerDataModel;
import com.memetro.android.api.ResultState;

import java.util.List;


public class BannerRepository {

    private final BannerRemoteDataSource bannerRemoteDataSource;

    public BannerRepository(BannerRemoteDataSource bannerRemoteDataSource) {
        this.bannerRemoteDataSource = bannerRemoteDataSource;
    }

    public ResultState<List<BannerDataModel>> getBanner( ) {
        ResultState<List<BannerDataModel>> result = bannerRemoteDataSource.getBanner();
        return result;
    }
}
