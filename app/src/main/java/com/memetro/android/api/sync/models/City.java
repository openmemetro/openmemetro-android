package com.memetro.android.api.sync.models;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "City",
        indices = {
                @Index(value = {"id"}, unique = true), @Index(value = {"country_id"})
        },
        foreignKeys = {
                @ForeignKey(entity = Country.class, parentColumns = {"id"}, childColumns = {"country_id"})
        }
)
public class City {

    @ColumnInfo(name = "name")
    @SerializedName("name")
    String name;

    @PrimaryKey
    @ColumnInfo(name = "id")
    @SerializedName("id")
    Integer id;

    @ColumnInfo(name = "country_id")
    @SerializedName("country_id")
    Integer countryId;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public Integer getCountryId() {
        return countryId;
    }

    @NonNull
    @Override
    public String toString() {
        return name;
    }
}
