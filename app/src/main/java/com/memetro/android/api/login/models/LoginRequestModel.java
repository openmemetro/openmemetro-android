package com.memetro.android.api.login.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LoginRequestModel implements Serializable {

    @SerializedName("username")
    String username;

    @SerializedName("password")
    String password;

    @SerializedName("device_id")
    String deviceId;

    public void setUsername(String username) {
        this.username = username;
    }
    public String getUsername() {
        return username;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public String getPassword() {
        return password;
    }

    public String getDeviceId() { return deviceId; }
    public void setDeviceId(String deviceId) { this.deviceId = deviceId; }
}
