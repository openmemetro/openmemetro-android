package com.memetro.android.api.sync.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Stations {
    @SerializedName("data")
    List<Station> data;

    public List<Station> getData() {
        return data;
    }

    public void setData(List<Station> data) {
        this.data = data;
    }
}

