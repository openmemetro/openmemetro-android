package com.memetro.android.api.captcha;

import com.memetro.android.api.ResultState;
import com.memetro.android.api.captcha.models.CaptchaDataModel;
//import com.memetro.android.api.banner.models.BannerDataModel;

import java.util.List;


public class CaptchaRepository {

    private final CaptchaRemoteDataSource captchaRemoteDataSource;

    public CaptchaRepository(CaptchaRemoteDataSource captchaRemoteDataSource) {
        this.captchaRemoteDataSource = captchaRemoteDataSource;
    }

    public ResultState<CaptchaDataModel> getCaptcha( ) {
        ResultState<CaptchaDataModel> result = captchaRemoteDataSource.getCaptcha();
        return result;
    }
}
