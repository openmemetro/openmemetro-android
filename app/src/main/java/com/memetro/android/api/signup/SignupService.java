package com.memetro.android.api.signup;

import com.memetro.android.api.signup.models.SignupRequestModel;
import com.memetro.android.api.signup.models.SignupResponseModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface SignupService {

    @POST("/signup/")
    Call<SignupResponseModel> register(
            @Body SignupRequestModel signupRequestModel
    );

}
