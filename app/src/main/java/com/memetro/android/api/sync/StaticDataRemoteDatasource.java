package com.memetro.android.api.sync;

import com.memetro.android.api.BaseRemoteDatasource;
import com.memetro.android.api.ResultState;
import com.memetro.android.api.sync.models.StaticDataResponseModel;

public class StaticDataRemoteDatasource extends BaseRemoteDatasource {

    private final StaticDataService service;

    public StaticDataRemoteDatasource(StaticDataService service) {
        this.service = service;
    }

    public ResultState<StaticDataResponseModel> sync() {
        return getResult(service.sync());
    }
}
