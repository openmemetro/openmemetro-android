package com.memetro.android.api.enviroments;

import com.memetro.android.api.enviroments.model.EnvListModel;
import com.memetro.android.api.sync.models.StaticDataResponseModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface EnvSearchService {

    @GET("env.json")
    Call<EnvListModel> getEnv();

    @GET("/StaticData")
    Call<StaticDataResponseModel> check();

}
