package com.memetro.android.api.signup.models;

import com.google.gson.annotations.SerializedName;

public class SignupResponseModel {

    @SerializedName("message")
    String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
