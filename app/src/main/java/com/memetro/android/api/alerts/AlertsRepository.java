package com.memetro.android.api.alerts;

import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.memetro.android.api.ResultState;
import com.memetro.android.api.alerts.models.Alert;
import com.memetro.android.api.alerts.models.AlertCreationRequest;
import com.memetro.android.api.alerts.models.AlertCreationResponse;
import com.memetro.android.api.alerts.models.AlertsListResponse;
import com.memetro.android.local.dao.models.UIAlert;
import com.memetro.android.local.datasource.alert.AlertsLocalDatasource;
import com.memetro.android.notifications.AlertNewNotificationsUtil;

import java.util.List;

public class AlertsRepository {
    private final AlertsRemoteDatasource alertsRemoteDatasource;
    private final AlertsLocalDatasource alertsLocalDatasource;
    private final AlertNewNotificationsUtil alertNewNotificationsUtil;

    public AlertsRepository(AlertsRemoteDatasource alertsRemoteDatasource, AlertsLocalDatasource alertsLocalDatasource, AlertNewNotificationsUtil alertNewNotificationsUtil) {
        this.alertsRemoteDatasource = alertsRemoteDatasource;
        this.alertsLocalDatasource = alertsLocalDatasource;
        this.alertNewNotificationsUtil = alertNewNotificationsUtil;
    }

    public List<UIAlert> getAlerts() {
        return alertsLocalDatasource.getAllAlerts();
    }

    public void getAlertsSync(Boolean showNotification) {
        ResultState<List<Alert>> alertsListResponse = alertsRemoteDatasource.getAlerts();
        switch (alertsListResponse.getStatus()) {
            case SUCCESS:
                if(alertsListResponse.getData() != null && !alertsListResponse.getData().isEmpty()) {
                    if(showNotification) {
                        int id = alertsListResponse.getData().get(alertsListResponse.getData().size()-1).getId();
                        if (id > alertsLocalDatasource.getLastAlert().getId()) {
                            alertNewNotificationsUtil.showNotification();
                        }
                    }
                    alertsLocalDatasource.insertAllAlerts(alertsListResponse.getData());
                }
                break;
            case ERROR:
                // TODO handle broadcast errors
                break;
        }
    }

    public ResultState<AlertCreationResponse> createAlert(
            AlertCreationRequest request
    ) {
        return alertsRemoteDatasource.createAlert(request);
    }

}
