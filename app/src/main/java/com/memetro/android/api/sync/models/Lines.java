package com.memetro.android.api.sync.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class Lines {
    @SerializedName("data")
    List<Line> data;

    public void setData(List<Line> data) {
        this.data = data;
    }

    public List<Line> getData() {
        return data;
    }
}

