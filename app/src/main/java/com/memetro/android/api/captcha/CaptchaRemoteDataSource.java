package com.memetro.android.api.captcha;

import com.memetro.android.api.BaseRemoteDatasource;
import com.memetro.android.api.ResultState;
import com.memetro.android.api.captcha.models.CaptchaDataModel;

import java.util.List;

public class CaptchaRemoteDataSource extends BaseRemoteDatasource {
    private final CaptchaService captchaService;

    public CaptchaRemoteDataSource(CaptchaService captchaService) {
        this.captchaService = captchaService;
    }

    public ResultState<CaptchaDataModel> getCaptcha( ) {
        return getResult(captchaService.getCaptcha());
    }
}
