package com.memetro.android.api.captcha.models;

import com.google.gson.annotations.SerializedName;

public class CaptchaDataModel {

    @SerializedName("captcha_key")
    String captchaKey;

    @SerializedName("captcha_image")
    String captchaImage;

    @SerializedName("image_type")
    String imageType;

    @SerializedName("image_decode")
    String imageDecode;

    public String getCaptchaKey() {
        return captchaKey;
    }

    public void setCaptchaKey(String captchaKey) {
        this.captchaKey = captchaKey;
    }

    public String getCaptchaImage() {
        return captchaImage;
    }

    public void setCaptchaImage(String captchaImage) {
        this.captchaImage = captchaImage;
    }

    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }

    public String getImageDecode() {
        return imageDecode;
    }

    public void setImageDecode(String imageDecode) {
        this.imageDecode = imageDecode;
    }
}
