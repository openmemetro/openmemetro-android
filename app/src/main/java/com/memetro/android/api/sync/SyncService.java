package com.memetro.android.api.sync;

import com.memetro.android.api.sync.models.SyncResponseModel;

import retrofit2.Call;
import retrofit2.http.GET;

public interface SyncService {

    @GET("/synchronize")
    Call<SyncResponseModel> sync();

}
