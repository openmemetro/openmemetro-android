package com.memetro.android.api.sync.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@Entity(
        tableName = "AlertType",
        indices = {
                @Index(value = {"id"}, unique = true),
                @Index(value = {"name"}),
                @Index(value = {"color"})
        }
)
public class AlertType {

    @PrimaryKey()
    @SerializedName("pk")
    Integer id;

    @ColumnInfo(name = "name")
    @SerializedName("name")
    String name;

    @ColumnInfo(name = "color")
    @SerializedName("color")
    String color;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return color + " " + name;
    }
}