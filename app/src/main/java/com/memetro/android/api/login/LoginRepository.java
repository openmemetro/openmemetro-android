package com.memetro.android.api.login;

import com.memetro.android.api.ResultState;
import com.memetro.android.api.login.models.LoginRequestModel;
import com.memetro.android.api.login.models.LoginResponseModel;
import com.memetro.android.api.login.models.RefreshTokenModel;
import com.memetro.android.sharedprefs.SharedPrefs;

public class LoginRepository {

    private final SharedPrefs sharedPrefs;
    private final LoginRemoteDatasource loginDatasource;

    public LoginRepository(
            LoginRemoteDatasource loginDatasource,
            SharedPrefs sharedPrefs
                           ) {
        this.sharedPrefs = sharedPrefs;
        this.loginDatasource = loginDatasource;
    }

    public ResultState<LoginResponseModel> login(LoginRequestModel loginRequst) {
        ResultState<LoginResponseModel> response = loginDatasource.login(loginRequst);
        if(response.getStatus() == ResultState.Status.SUCCESS && response.getData() != null) {
            sharedPrefs.saveToken(response.getData().getAccessToken());
            sharedPrefs.saveRefreshToken(response.getData().getRefreshToken());
        }
        return response;
    }

    public ResultState<LoginResponseModel> autoLogin() {
        if(sharedPrefs.getRefreshToken() != null) {
            RefreshTokenModel refreshTokenModel = new RefreshTokenModel();
            refreshTokenModel.setRefreshToken(sharedPrefs.getRefreshToken());
            ResultState<LoginResponseModel> response = loginDatasource.autoLogin(refreshTokenModel);
            if(response.getStatus() == ResultState.Status.SUCCESS && response.getData() != null) {
                sharedPrefs.saveToken(response.getData().getAccessToken());
                sharedPrefs.saveRefreshToken(response.getData().getRefreshToken());
            }
            return response;
        }
        return ResultState.error("No session", -1);
    }

    public void setAutologin(boolean autologin) {
        sharedPrefs.saveAutoLogin(autologin);
    }

    public boolean isAutoLoginEnabled() {
        return sharedPrefs.getAutoLogin();
    }

}
