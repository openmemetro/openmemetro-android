package com.memetro.android.api;

import retrofit2.Call;
import retrofit2.Response;

public abstract class BaseRemoteDatasource {

    protected <T> ResultState<T> getResult(Call<T> endpoint) {
        try {
            Response<T> response = endpoint.execute();
            if(response.isSuccessful()) {
                T body = response.body();
                if(body!=null) return ResultState.success(body);
            }
            return ResultState.error(response.errorBody().toString(), response.code());
        } catch (Exception e) {
            return ResultState.error(e.getMessage(), -1);
        }
    }

    protected ResultState getResultVoid(Call<Void> endpoint) {
        try {
            Response<Void> response = endpoint.execute();
            if(response.isSuccessful()) {
                return ResultState.success(null);
            }
            return ResultState.error(response.errorBody().toString(), response.code());
        } catch (Exception e) {
            return ResultState.error(e.getMessage(), -1);
        }
    }
}
