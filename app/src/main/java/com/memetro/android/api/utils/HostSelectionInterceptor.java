package com.memetro.android.api.utils;

import android.os.AsyncTask;

import androidx.annotation.NonNull;

import com.memetro.android.api.ResultState;
import com.memetro.android.api.enviroments.EnvSearchRemoteDatasource;
import com.memetro.android.api.enviroments.model.EnvListModel;
import com.memetro.android.api.sync.models.StaticDataResponseModel;
import com.memetro.android.sharedprefs.SharedPrefs;

import java.io.IOException;
import java.net.URISyntaxException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;

public class HostSelectionInterceptor implements Interceptor {

    private volatile HttpUrl host = null;
    private final EnvSearchRemoteDatasource envSearchDatasource;

    public HostSelectionInterceptor(
            EnvSearchRemoteDatasource envSearchDatasource){
        this.envSearchDatasource = envSearchDatasource;
        findLiveHosts();
    }

    public void findLiveHosts() {
        AsyncTask.execute(() ->{
            ResultState<StaticDataResponseModel> check = envSearchDatasource.check();
            if(check.getStatus() != ResultState.Status.SUCCESS) {
                EnvListModel envListModel = envSearchDatasource.getEnv().getData();
                if (envListModel != null) {
                    for(String env: envListModel.getEnv()) {
                        host = HttpUrl.parse(env);
                        check = envSearchDatasource.check();
                        if(check.getStatus() == ResultState.Status.SUCCESS) {
                            break;
                        }
                    }
                }
            }
        });
    }

    @NonNull
    @Override
    public okhttp3.Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        HttpUrl newUrl = null;
        try {
            newUrl = request.url().newBuilder()
                    .scheme(host.scheme())
                    .host(host.url().toURI().getHost())
                    .build();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        assert newUrl != null;
        request = request.newBuilder()
                .url(newUrl)
                .build();
        return chain.proceed(request);
    }
}
