package com.memetro.android.api.banner.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BannerResponseModel {

    @SerializedName("banners")
    List<BannerDataModel> banners;

    public List<BannerDataModel> getBanners() {
        return banners;
    }

    public void setBanners(List<BannerDataModel> banners) {
        this.banners = banners;
    }
}
