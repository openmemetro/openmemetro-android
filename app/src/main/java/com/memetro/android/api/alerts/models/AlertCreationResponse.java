package com.memetro.android.api.alerts.models;

import com.google.gson.annotations.SerializedName;

public class AlertCreationResponse {

    @SerializedName("message")
    String message;

    public void setMessage(String message) {
        this.message = message;
    }
    public String getMessage() {
        return message;
    }

}
