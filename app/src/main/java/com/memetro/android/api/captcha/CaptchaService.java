package com.memetro.android.api.captcha;

import com.memetro.android.api.captcha.models.CaptchaDataModel;
import retrofit2.Call;
import retrofit2.http.POST;

public interface CaptchaService {
    @POST("/api/captcha/")
    Call<CaptchaDataModel> getCaptcha();
}
