package com.memetro.android.api.sync.models;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.memetro.android.R;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "Transport", indices = {@Index(value = {"id"}, unique = true)})
public class Transport {
    @ColumnInfo(name = "name")
    @SerializedName("name")
    String name;

    @ColumnInfo(name = "id")
    @PrimaryKey
    @SerializedName("id")
    Integer id;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public IconRef getIcon() {
        switch (this.name) {
            case  "Bus":
                return IconRef.BUS;
            case  "FGC":
                return IconRef.CERCANIAS;
            case  "Tram":
                return IconRef.COLGANTE;
            case  "Renfe":
                return IconRef.MEDIA_DISTANCIA;
            case  "Metro":
                return IconRef.METRO;
            default:
                return IconRef.DEFAULT;
        }
    }

    @NonNull
    @Override
    public String toString() {
        return name;
    }

    public enum IconRef {
        BUS(R.drawable.icon_bus),
        CERCANIAS(R.drawable.icon_cercanias),
        COLGANTE(R.drawable.icon_colgante),
        MEDIA_DISTANCIA(R.drawable.icon_media_distancia),
        METRO(R.drawable.icon_metro),
        DEFAULT(R.drawable.icon_metro);

        public final Integer iconRef;
        private IconRef(Integer iconRef) {
            this.iconRef = iconRef;
        }
    }
}
