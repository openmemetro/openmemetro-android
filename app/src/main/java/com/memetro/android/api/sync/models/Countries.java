package com.memetro.android.api.sync.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class Countries {
    @SerializedName("data")
    List<Country> data;

    public void setData(List<Country> data) {
        this.data = data;
    }

    public List<Country> getData() {
        return data;
    }
}

