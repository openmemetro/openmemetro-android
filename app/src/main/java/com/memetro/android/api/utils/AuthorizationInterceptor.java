package com.memetro.android.api.utils;

import com.memetro.android.BuildConfig;
import com.memetro.android.sharedprefs.SharedPrefs;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class AuthorizationInterceptor implements Interceptor {

    private final SharedPrefs sharedPrefs;

    public AuthorizationInterceptor(SharedPrefs sharedPrefs) {
        this.sharedPrefs = sharedPrefs;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request newRequest = signedRequest(chain.request());
        if(newRequest!=null) {
            if(!BuildConfig.OAUTHSERVER.contains(newRequest.url().host())) {
                return chain.proceed(chain.request());
            } else if(sharedPrefs.getToken() != null) {
                return chain.proceed(newRequest);
            }
        }
        return chain.proceed(chain.request());
    }

    public Request signedRequest(Request request) {
        if(!BuildConfig.OAUTHSERVER.contains(request.url().host())) {
            return null;
        } else if(sharedPrefs.getToken() != null) {
            return request.newBuilder()
                    .header("Authorization", "Bearer "+sharedPrefs.getToken())
                    .build();
        }
        return null;
    }
}


