package com.memetro.android.api.sync.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Cities {
    @SerializedName("data")
    List<City> data;

    public void setData(List<City> data) {
        this.data = data;
    }

    public List<City> getData() {
        return data;
    }
}


