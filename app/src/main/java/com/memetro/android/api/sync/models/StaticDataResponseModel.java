package com.memetro.android.api.sync.models;

import com.google.gson.annotations.SerializedName;


public class StaticDataResponseModel {
    @SerializedName("Country")
    Countries countries;

    @SerializedName("City")
    Cities cities;

    public Cities getCities() {
        return cities;
    }

    public void setCities(Cities cities) {
        this.cities = cities;
    }

    public Countries getCountries() {
        return countries;
    }

    public void setCountries(Countries countries) {
        this.countries = countries;
    }

}

