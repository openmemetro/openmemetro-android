package com.memetro.android.api.sync.models;

import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("name")
    String name;

    @SerializedName("username")
    String username;

    @SerializedName("email")
    String email;

    @SerializedName("twittername")
    String twittername;

    @SerializedName("aboutme")
    String aboutme;

    @SerializedName("city_id")
    Integer cityId;

    @SerializedName("img_url")
    String imgUrl;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setTwittername(String twittername) {
        this.twittername = twittername;
    }

    public String getTwittername() {
        return twittername;
    }

    public void setAboutme(String aboutme) {
        this.aboutme = aboutme;
    }

    public String getAboutme() {
        return aboutme;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public Integer getCityId() {
        return cityId;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
}

