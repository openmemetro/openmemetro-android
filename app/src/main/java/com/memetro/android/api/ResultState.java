package com.memetro.android.api;

public class ResultState<T>{

    private final Status status;
    private final T data;
    private final Integer code;
    private final String message;

    public ResultState(Status status, T data, Integer code, String message) {
        this.status = status;
        this.data = data;
        this.code = code;
        this.message = message;
    }

    public static <T> ResultState<T> success(T data) {
        return new ResultState<T>(Status.SUCCESS, data, null, null);
    }
    public static <T> ResultState<T> error(String message, Integer code) {
        return new ResultState<T>(Status.ERROR, null, code, message);
    }
    public static <T> ResultState<T> loading() {
        return new ResultState<T>(Status.LOADING, null, null, null);
    }
    public static <T> ResultState<T> loaded(T data) {
        return new ResultState<T>(Status.LOADED, data, null, null);
    }

    public enum Status {
        SUCCESS, ERROR, LOADING, LOADED
    }

    public Status getStatus() {
        return status;
    }

    public T getData() {
        return data;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
