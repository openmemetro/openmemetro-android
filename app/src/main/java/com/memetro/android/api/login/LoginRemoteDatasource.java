package com.memetro.android.api.login;

import com.memetro.android.api.BaseRemoteDatasource;
import com.memetro.android.api.ResultState;
import com.memetro.android.api.login.models.LoginRequestModel;
import com.memetro.android.api.login.models.LoginResponseModel;
import com.memetro.android.api.login.models.RefreshTokenModel;

public class LoginRemoteDatasource extends BaseRemoteDatasource {

    private final LoginService oAuthService;
    private final RefreshTokenService refreshTokenService;


    public LoginRemoteDatasource(LoginService oAuthService, RefreshTokenService refreshTokenService) {
        this.oAuthService = oAuthService;
        this.refreshTokenService = refreshTokenService;
    }

    public ResultState<LoginResponseModel> login(LoginRequestModel requestModel) {
        return getResult(oAuthService.login(requestModel));
    }

    public ResultState<LoginResponseModel> autoLogin(RefreshTokenModel refreshTokenRequest) {
        return getResult(refreshTokenService.refreshToken(refreshTokenRequest));
    }

}
