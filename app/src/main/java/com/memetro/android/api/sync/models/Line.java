package com.memetro.android.api.sync.models;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@Entity(
        tableName = "Line",
        indices = {
                @Index(value = {"id"}, unique = true),
                @Index(value = {"transport_id"}),
                @Index(value = {"city_id"})
        },
        foreignKeys = {
                //@ForeignKey(entity = CitiesTransport.class, parentColumns = "transport_id", childColumns = "transport_id")
                @ForeignKey(entity = City.class, parentColumns = "id", childColumns = "city_id"),
                @ForeignKey(entity = Transport.class, parentColumns = "id", childColumns = "transport_id")
        }
)
public class Line {

    @PrimaryKey
    @SerializedName("id")
    @ColumnInfo(name = "id")
    Integer id;

    @SerializedName("name")
    @ColumnInfo(name = "name")
    String name;

    @SerializedName("city")
    @ColumnInfo(name = "city_id")
    Integer cityId;

    //TODO FK STATION-LINE
    //@SerializedName("stations")
    //@Ignore
    //List<Integer> stations;

    @SerializedName("transport_id")
    @ColumnInfo(name = "transport_id")
    Integer transportId;


    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }
    public Integer getCityId() {
        return cityId;
    }

    /*public void setStations(List<Integer> stations) {
        this.stations = stations;
    }
    public List<Integer> getStations() {
        return stations;
    }*/

    public void setTransportId(Integer transportId) {
        this.transportId = transportId;
    }
    public Integer getTransportId() {
        return transportId;
    }

    @NonNull
    @Override
    public String toString() {
        return this.name;
    }
}
