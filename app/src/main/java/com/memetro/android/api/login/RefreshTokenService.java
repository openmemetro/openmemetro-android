package com.memetro.android.api.login;

import com.memetro.android.api.login.models.LoginResponseModel;
import com.memetro.android.api.login.models.RefreshTokenModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface RefreshTokenService {

    @POST("api/token/refresh/")
    Call<LoginResponseModel> refreshToken(
            @Body RefreshTokenModel refreshTokenRequest
    );
}
