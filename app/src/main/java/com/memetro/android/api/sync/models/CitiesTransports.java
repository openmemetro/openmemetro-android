package com.memetro.android.api.sync.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CitiesTransports {
    @SerializedName("data")
    List<CitiesTransport> data;

    public void setData(List<CitiesTransport> data) {
        this.data = data;
    }

    public List<CitiesTransport> getData() {
        return data;
    }
}

