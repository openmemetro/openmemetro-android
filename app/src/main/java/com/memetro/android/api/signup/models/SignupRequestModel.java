package com.memetro.android.api.signup.models;

import com.google.gson.annotations.SerializedName;

public class SignupRequestModel {

    @SerializedName("username")
    String username;

    @SerializedName("password")
    String password;

    @SerializedName("country_id")
    int countryId;

    @SerializedName("city_id")
    int cityId;

    @SerializedName("device_id")
    String deviceId;

    @SerializedName("captcha_key")
    String captchaKey;

    @SerializedName("captcha_value")
    String captchaValue;


    public String getCaptchaKey() {
        return captchaKey;
    }

    public void setCaptchaKey(String captchaKey) {
        this.captchaKey = captchaKey;
    }

    public String getCaptchaValue() {
        return captchaValue;
    }

    public void setCaptchaValue(String captchaValue) {
        this.captchaValue = captchaValue;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getCountryId() { return countryId; }

    public void setCountryId(int countryId) { this.countryId = countryId; }

    public int getCityId() { return cityId; }

    public void setCityId(int cityId) { this.cityId = cityId; }

    public String getDeviceId() { return deviceId; }

    public void setDeviceId(String deviceId) { this.deviceId = deviceId; }

}
