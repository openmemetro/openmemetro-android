package com.memetro.android.api.sync;

import com.memetro.android.api.sync.models.StaticDataResponseModel;
import retrofit2.Call;
import retrofit2.http.GET;

public interface StaticDataService {
    @GET("/StaticData")
    Call<StaticDataResponseModel> sync();
}
