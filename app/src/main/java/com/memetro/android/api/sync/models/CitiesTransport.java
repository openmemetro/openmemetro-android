package com.memetro.android.api.sync.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "CitiesTransport",
        indices = {
            @Index(value = {"transport_id", "city_id"}, unique = true),
            @Index(value = {"transport_id"}),
            @Index(value = {"city_id"})
        },
        foreignKeys = {
                @ForeignKey(entity = City.class, parentColumns = {"id"}, childColumns = {"city_id"}),
                @ForeignKey(entity = Transport.class, parentColumns = {"id"}, childColumns = {"transport_id"})
        }

)
public class CitiesTransport {

    @PrimaryKey
    @ColumnInfo(name = "transport_id")
    @SerializedName("transport_id")
    Integer transportId;

    @ColumnInfo(name = "city_id")
    @SerializedName("city_id")
    Integer cityId;

    public void setTransportId(Integer transportId) {
        this.transportId = transportId;
    }
    public Integer getTransportId() {
        return transportId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }
    public Integer getCityId() {
        return cityId;
    }
}
