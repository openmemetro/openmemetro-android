package com.memetro.android.sharedprefs;

import com.memetro.android.api.sync.models.User;

public interface SharedPrefs {

    String SHARED_PREFS_NAME = "SHARED_PREFS_NAME_";
    String SHARED_PREFS_KEY_TOKEN = "SHARED_PREFS_KEY_TOKEN_";
    String SHARED_PREFS_KEY_REFRESH_TOKEN = "SHARED_PREFS_KEY_REFRESH_TOKEN_";
    String SHARED_PREFS_KEY_LAST_HOST = "SHARED_PREFS_KEY_LAST_HOST_";
    String SHARED_PREFS_KEY_USER_NAME = "SHARED_PREFS_KEY_USER_NAME_";
    String SHARED_PREFS_KEY_USER_USERNAME = "SHARED_PREFS_KEY_USER_USERNAME_";
    String SHARED_PREFS_KEY_USER_EMAIL = "SHARED_PREFS_KEY_USER_EMAIL_";
    String SHARED_PREFS_KEY_USER_TWITTER = "SHARED_PREFS_KEY_USER_TWITTER_";
    String SHARED_PREFS_KEY_USER_ABOUT_ME = "SHARED_PREFS_KEY_USER_ABOUT_ME_";
    String SHARED_PREFS_KEY_USER_CITY_ID = "SHARED_PREFS_KEY_USER_CITY_ID_";
    String SHARED_PREFS_KEY_AUTO_LOGIN = "SHARED_PREFS_KEY_AUTO_LOGIN_";
    String SHARED_PREFS_KEY_DEVICE_ID = "SHARED_PREFS_KEY_DEVICE_ID_";

    String TRUE = "1";
    String FALSE = "0";

    void clearUserData();

    void saveToken(String token);
    void saveRefreshToken(String token);
    void saveAutoLogin(boolean autoLogin);
    void saveUser(User user);
    void saveDeviceId(String userHash);
    String getToken();
    String getRefreshToken();
    String getName();
    String getUserName();
    String getEmail();
    String getTwitterName();
    String getAboutMe();
    Integer getCityId();
    User getUser();
    boolean getAutoLogin();
    String getDeviceId();

}
