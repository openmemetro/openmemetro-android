package com.memetro.android.sharedprefs;

import android.content.SharedPreferences;

import com.memetro.android.api.sync.models.User;

public class SharedPrefsImpl implements SharedPrefs {

    private final SharedPreferences sharedPreferences;

    public SharedPrefsImpl(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    @Override
    public void clearUserData() {
        sharedPreferences.edit()
                .putString(SHARED_PREFS_KEY_TOKEN, null)
                .putString(SHARED_PREFS_KEY_REFRESH_TOKEN, null)
                .putString(SHARED_PREFS_KEY_USER_NAME, null)
                .putString(SHARED_PREFS_KEY_USER_USERNAME, null)
                .putString(SHARED_PREFS_KEY_USER_EMAIL, null)
                .putString(SHARED_PREFS_KEY_USER_TWITTER, null)
                .putString(SHARED_PREFS_KEY_USER_ABOUT_ME, null)
                .putInt(SHARED_PREFS_KEY_USER_CITY_ID, -1)
                .putBoolean(SHARED_PREFS_KEY_AUTO_LOGIN, false)
                .putString(SHARED_PREFS_KEY_DEVICE_ID, null)
                .apply();
    }

    @Override
    public void saveToken(String token) {
        sharedPreferences.edit().putString(SHARED_PREFS_KEY_TOKEN, token).apply();
    }

    @Override
    public void saveRefreshToken(String token) {
        sharedPreferences.edit().putString(SHARED_PREFS_KEY_REFRESH_TOKEN, token).apply();
    }

    @Override
    public void saveAutoLogin(boolean autoLogin) {
        sharedPreferences.edit().putString(SHARED_PREFS_KEY_AUTO_LOGIN, autoLogin ? TRUE : FALSE).apply();
    }

    @Override
    public void saveUser(User user) {
        sharedPreferences.edit()
                .putString(SHARED_PREFS_KEY_USER_NAME, user.getName())
                .putString(SHARED_PREFS_KEY_USER_USERNAME, user.getUsername())
                .putString(SHARED_PREFS_KEY_USER_EMAIL, user.getEmail())
                .putString(SHARED_PREFS_KEY_USER_TWITTER, user.getTwittername())
                .putString(SHARED_PREFS_KEY_USER_ABOUT_ME, user.getAboutme())
                .putInt(SHARED_PREFS_KEY_USER_CITY_ID, user.getCityId())
                .apply();
    }

    @Override
    public String getToken() {
        return sharedPreferences.getString(SHARED_PREFS_KEY_TOKEN, null);
    }

    @Override
    public String getRefreshToken() {
        return sharedPreferences.getString(SHARED_PREFS_KEY_REFRESH_TOKEN, null);
    }

    @Override
    public String getName() {
        return sharedPreferences.getString(SHARED_PREFS_KEY_USER_NAME, null);
    }

    @Override
    public String getUserName() {
        return sharedPreferences.getString(SHARED_PREFS_KEY_USER_USERNAME, null);
    }

    @Override
    public String getEmail() {
        return sharedPreferences.getString(SHARED_PREFS_KEY_USER_EMAIL, null);
    }

    @Override
    public String getTwitterName() {
        return sharedPreferences.getString(SHARED_PREFS_KEY_USER_TWITTER, null);
    }

    @Override
    public String getAboutMe() {
        return sharedPreferences.getString(SHARED_PREFS_KEY_USER_ABOUT_ME, null);
    }

    @Override
    public Integer getCityId() {
        return sharedPreferences.getInt(SHARED_PREFS_KEY_USER_CITY_ID, -1);
    }

    @Override
    public User getUser() {
        User user = new User();
        user.setName(this.getName());
        user.setUsername(this.getUserName());
        user.setEmail(this.getEmail());
        user.setTwittername(this.getTwitterName());
        user.setAboutme(this.getAboutMe());
        user.setCityId(this.getCityId());
        return user;
    }

    @Override
    public boolean getAutoLogin() {
        return sharedPreferences.getString(SHARED_PREFS_KEY_AUTO_LOGIN, FALSE).equals(TRUE);
    }

    @Override
    public void saveDeviceId(String userHash) {
        sharedPreferences.edit().putString(SHARED_PREFS_KEY_DEVICE_ID, userHash).apply();
    }

    @Override
    public String getDeviceId() {
        return sharedPreferences.getString(SHARED_PREFS_KEY_DEVICE_ID, null);
    }
}
